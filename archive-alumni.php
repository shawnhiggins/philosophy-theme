<?php get_header(); ?>
			<div class="content main">
				<header>
					<h1>Alumni</h1>
				</header>
				<div class="book-list">
					<ul <?php post_class('cf'); ?>>
					<?php $book_loop = new WP_Query( array( 'post_type' => 'alumni', 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'desc',
						'meta_query' => array( 
							array(
								'key' => 'show_on_alumni_page',
								'value' => '1',
								'compare' => '=='
							) 
						)
					 ) ); ?>
					<?php while ( $book_loop->have_posts() ) : $book_loop->the_post(); ?>
						<li>
							<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
							<?php if(get_field('book_cover')) {
								$image = get_field('book_cover');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'medium-book';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> book cover" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="cover" />
								<?php } else { ?>
								<div class="custom-cover cover">
									<span class="title"><?php the_title(); ?></span>
								</div>
								<?php } ?>
							</a>									
							<dl>
								<dt class="title"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></dt>
								<?php if(get_field('publisher')) { ?>
								<dd class="publisher"><?php the_field('publisher'); ?>, <?php the_field('published_date'); ?></dd>
								<?php } ?>
								<dd class="author">
									<?php $author = get_field('author'); ?>
									<? if( $author ): ?>
									<?php foreach( $author as $post): ?>
									<?php setup_postdata($post); ?>
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									<?php endforeach; ?>
									<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</dd>
							</dl>
						</li>
					<?php endwhile; ?>
					</ul>
				</div>
			</div>
<?php get_footer(); ?>