<?php
/*
 Template Name: Home Page
*/
?>
<?php get_header(); ?>
			<div id="main-content" role="main">
				<?php // If set to Single/Random 
					if(get_field('hero_type', 'option') == "single") {
					$rows = get_field('hero_image' ); // get all the rows
					$rand_row = $rows[ array_rand( $rows ) ]; // get a random row
					$silder_image = $rand_row['image' ];
					$slider_description = $rand_row['description' ];
					$slider_link = $rand_row['link' ];
					if( !empty($silder_image) ): 
						// vars
						$title = $silder_image['title'];
						// thumbnail
						$size = 'home-hero';
						$slide = $silder_image['sizes'][ $size ];
						$width = $silder_image['sizes'][ $size . '-width' ];
						$height = $silder_image['sizes'][ $size . '-height' ];
					endif;
				?>
				<?php if( $silder_image ): ?>
					<?php if( $slider_link ): ?>
					<a href="<?php echo $home_button_link; ?>" class="hero-link">
					<?php endif; ?>
						<div id="hero" class="desktop" style="background-image: url('<?php echo $slide; ?>');">
							<div class="content">
							<?php if( $slider_description ): ?>
								<div class="hero-container">
									<div class="hero-description">
										<p><?php echo $slider_description; ?></p>
									</div>
								</div>
							<?php endif; ?>
							</div>
						</div>
						<div id="hero" class="mobile-hero" style="background-image:url('<?php echo $slide; ?>');">
						</div>
					<?php if( $slider_link ): ?>
					</a>
					<?php endif; ?>
				<?php endif; ?>
				<?php } ?>
				
				<?php // If set to Slider
					if(get_field('hero_type', 'option') == "slider") { ?>
					<script type="text/javascript">
						jQuery("document").ready(function($) {
							$(document).ready(function(){
							  $('#bxslider').bxSlider({
							  	autoHover: true,
							  	auto: false,
							  });
							});
						});
					</script>
					<div id="slider">
						<ul id="bxslider">
							<?php if( have_rows('hero_image') ): ?>
							<?php while( have_rows('hero_image') ): the_row(); ?>
							<?php
								$slider_title = get_sub_field('title');
								$slider_description = get_sub_field('description');
								$slider_link = get_sub_field('link');
								$silder_image = get_sub_field('image');
								if( !empty($silder_image) ): 
									// vars
									$url = $silder_image['url'];
									$title = $silder_image['title'];
									// thumbnail
									$size = 'home-hero';
									$slide = $silder_image['sizes'][ $size ];
									$width = $silder_image['sizes'][ $size . '-width' ];
									$height = $silder_image['sizes'][ $size . '-height' ];
								endif;
							?>		
							<?php if( $slider_link ): ?>
							<a href="<?php echo $slider_link; ?>" class="hero-link">
							<?php endif; ?>
							<li style="background-image: url('<?php echo $slide; ?>');">
								<div class="content">
									<div class="slider-content">
										<?php if( $slider_title ): ?>
										<h2><?php echo $slider_title; ?></h2>
										<?php endif; ?>
										<?php if( $slider_description ): ?>
										<p><?php echo $slider_description; ?></p>
										<?php endif; ?>
									</div>
								</div>
							</li>
							<?php if( $slider_link ): ?>
							</a>
							<?php endif; ?>
							<?php endwhile; ?>
							<?php endif; ?>
						</ul>
					</div>
				<?php } ?>
				<div class="content">
					<?php
					if( have_rows('homepage_columns') ) :
						while ( have_rows('homepage_columns') ) : the_row();
							
							// For showing snippet from any page
							if( get_row_layout() == 'page_excerpt' ) 
								get_template_part('snippets/col', 'page');
					        
							// For showing list of recent post
							elseif( get_row_layout() == 'recent_posts' ) 
								get_template_part('snippets/col', 'posts');
							
							// For showing free form content
							elseif( get_row_layout() == 'content_block' ) 
								get_template_part('snippets/col', 'content');
							
							// For showing list of events from event widget
							elseif( get_row_layout() == 'upcoming_events' ) 
					       		get_template_part('snippets/col', 'events');
					       	
					       	// For showing a menu
					       	elseif( get_row_layout() == 'menu' ) 
					       			get_template_part('snippets/col', 'menu');
							
						endwhile;
					endif;
			    	?>
					</div>					
				</div>
			</div>
<?php get_footer(); ?>