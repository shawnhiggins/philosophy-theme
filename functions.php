<?php
// Load Bones Core
require_once( 'library/bones.php' );
	
// Customize Wordpress Admin
require_once( 'library/admin.php' );

// Recommend Plugins
require_once( 'library/install-plugins.php' );

/*********************
LAUNCH BONES
*********************/

function bones_ahoy() {

	// Allow editor style.
	add_editor_style( 'library/css/editor-style.css');
	
/************* CUSTOM POST TYPES *************/
	
	// Controlled by ACF plugin.
	// Check to see if the plugin is installed first.
	if( function_exists ('acf') ) {
		if(get_field('enable_people', 'option') == "enable") {
			require_once( 'library/post-types/people-post-type.php' );
		}
		if(get_field('enable_alumni', 'option') == "enable") {
			require_once( 'library/post-types/alumni-post-type.php' );
		}
		if(get_field('enable_courses', 'option') == "enable") {
			require_once( 'library/post-types/courses-post-type.php' );
		}
		if(get_field('enable_books', 'option') == "enable") {
			require_once( 'library/post-types/book-post-type.php' );
		}
		if(get_field('enable_conferences', 'option') == "enable") {
			require_once( 'library/post-types/conference-post-type.php' );
		}
		if(get_field('enable_newsletter', 'option') == "enable") {
			require_once( 'library/post-types/newsletter-post-type.php' );
		}
	}
	// If it's not, then just do it
	// Comment out if you don't need something.
	else {
		require_once( 'library/post-types/people-post-type.php' );
		require_once( 'library/post-types/alumni-post-type.php' );
		require_once( 'library/post-types/courses-post-type.php' );
		require_once( 'library/post-types/book-post-type.php' );
		require_once( 'library/post-types/conference-post-type.php' );
		require_once( 'library/post-types/newsletter-post-type.php' );
	}

/************* END *************/

	// launching operation cleanup
	add_action( 'init', 'bones_head_cleanup' );
	// A better title
	add_filter( 'wp_title', 'rw_title', 10, 3 );
	// remove WP version from RSS
	add_filter( 'the_generator', 'bones_rss_version' );
	// remove pesky injected css for recent comments widget
	add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
	// clean up comment styles in the head
	add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
	// clean up gallery output in wp
	add_filter( 'gallery_style', 'bones_gallery_style' );
	
	// enqueue base scripts and styles
	add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
		
	// launching this stuff after theme setup
	bones_theme_support();
	
	// adding sidebars to Wordpress (these are created in functions.php)
	add_action( 'widgets_init', 'bones_register_sidebars' );
	
	// cleaning up random code around images
	add_filter( 'the_content', 'bones_filter_ptags_on_images' );
} /* end bones ahoy */

// let's get this party started
add_action( 'after_setup_theme', 'bones_ahoy' );


/************* OEMBED SIZE OPTIONS *************/

if ( ! isset( $content_width ) ) {
	$content_width = 640;
}

/************* THUMBNAIL SIZE OPTIONS *************/

// Hero image
add_image_size( 'hero', 1400, 400, array( 'center', 'top' ) );
// Home Hero image
add_image_size( 'home-hero', 1400, 511, array( 'center', 'top' ) );
// Content width
add_image_size( 'content-width', 640, 430, true );
// Full Content width
add_image_size( 'full-content-width', 940, 268, true );
// Article thumbnail
add_image_size( 'article-thumb', 110, 70, true );
// Big people photo
add_image_size( 'people-large', 280, 280, array( 'center', 'top' ) );
// Small people photo
add_image_size( 'people-thumb', 210, 210, array( 'center', 'top' ) );
// Large book cover
add_image_size( 'large-book', 274, 383, true );
// Medium book cover
add_image_size( 'medium-book', 101, 157, true );
// Small book cover
add_image_size( 'small-book', 60, 90, true );
// Speaker photo
add_image_size( 'speaker-photo', 160, 160, true );
// Big Newsletter People photo
add_image_size( 'newsletter-people-photo', 140, 175, array( 'center', 'top' ) );

/* 
This makes Wordpress create thumbnails at these 
sizes for every image for you to then use in
theme as needed.

To add more sizes, simply copy a line and change 
the dimensions & name. As long as you upload a 
"featured image" as large as the biggest set 
width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 100 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
*/

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
	return array_merge( $sizes, array(
		'content-width' => __('Full Content Width'),
	) );
}

/*
The function above adds the ability to use the dropdown menu to select
the new images sizes you have just created from within the media manager
when you add media to your content blocks. If you add more image sizes,
duplicate one of the lines in the array and name it according to your
new image size.
*/

/************* SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'news-sidebar',
		'name' => __( 'News Sidebar', 'bonestheme' ),
		'description' => __( 'Sidebar for news article feed.', 'bonestheme' ),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => '</div><a class="btn" href="/category/news/">View All<span class="hidden"> News</span></a>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	
	register_sidebar(array(
		'id' => 'events-sidebar',
		'name' => __( 'Event Sidebar', 'bonestheme' ),
		'description' => __( 'Sidebar for event feed.', 'bonestheme' ),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	
	register_sidebar(array(
		'id' => 'give-sidebar',
		'name' => __( 'Give Sidebar', 'bonestheme' ),
		'description' => __( 'Sidebar for the Give page.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

} // don't remove this bracket!

/*********************
ADDITIONAL FUNCTIONS
*********************/

function prefix_nav_description( $item_output, $item, $depth, $args ) {
    if ( !empty( $item->description ) ) {
        $item_output = str_replace( $args->link_after . '</a>', '<span class="description">' . $item->description . '</span>' . $args->link_after . '</a>', $item_output );
    }
 
    return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'prefix_nav_description', 10, 4 );

/************* Filter Menu *********************/

// Removing classes and replacing anchor with button element

class Filter_Walker extends Walker_Nav_Menu {

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent\n";
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent\n";
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="option ' . esc_attr( $class_names ) . '"' : '';

		$output .= $indent . '';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ' data-filter=".'. esc_attr( $item->xfn   ) .'"';
		$attributes .= ' data-text="'. esc_attr( $item->title   ) .'"';

		$item_output = $args->before;
		$item_output .= '<button'. $attributes . $class_names .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</button>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "\n";
	}
}

// Removing all but custom classes
function modify_nav_menu_args( $args ) {
	if( 'faculty-filter' == $args['theme_location'] ) {
		add_filter('nav_menu_css_class', 'discard_menu_classes', 10, 2);
		function discard_menu_classes($classes, $item) {
			return (array)get_post_meta( $item->ID, '_menu_item_classes', true );
		}
	}
	return $args;
}
add_filter( 'wp_nav_menu_args', 'modify_nav_menu_args' );

/************* TARGETING PARENT PAGE *********************/

// Be able to target pages by using is_tree(parent-id)

function is_tree($pid) {
	global $post;
	
	$ancestors = get_post_ancestors($post->$pid);
	$root = count($ancestors) - 1;
	$parent = $ancestors[$root];
	
	if(is_page() && (is_page($pid) || $post->post_parent == $pid || in_array($pid, $ancestors))) {
		return true;
	}
	else {
		return false;
	}
};

/************* SHORTCODE *********************/

// Remove bad html in shortcode
function shortcode_empty_paragraph_fix($content) {
	$array = array (
		'<p>[' => '[',
		']</p>' => ']',
		']<br />' => ']'
	);
	$content = strtr($content, $array);
	return $content;
}
add_filter('the_content', 'shortcode_empty_paragraph_fix');

// FAQ Container
function faq_shortcode( $atts, $content = null ) {
	return '<dl class="faq">' . do_shortcode($content) . '</dl>';
}
add_shortcode( 'faq', 'faq_shortcode' );

// Question Shortcode
function question_shortcode( $atts, $content = null ) {
	extract( shortcode_atts(
		array(
			'title' => '',
		),
	$atts ) );
	return '<dt class="question">'. $title .'</dt><dd class="answer">' . do_shortcode($content) . '</dd>';
}
add_shortcode('question', 'question_shortcode');

/* Code Example
[faq]
[question title="To be or not to be?"]That is the question.[/question]
[/faq]
*/

// Button Shortcode
function button_shortcode( $atts, $content = null ) {
	extract( shortcode_atts(
		array(
			'link' => '',
		),
	$atts ) );
	return '<a href="' . $link . '" class="btn">' . $content . '</a>';
}
add_shortcode('button', 'button_shortcode');

/* Code Example
[button link="http://ucla.edu"]Button Text[/button]
*/

/*
// Language Shortcode
// Use the below code to let screen readers know when content is in a different language
// replace "es" with proper language code (http://www.w3schools.com/tags/ref_language_codes.asp)

// Spanish
function spanish_shortcode( $atts, $content = null ) {
	return '<span lang="es">' . $content . '</span>';
}
add_shortcode( 'spanish', 'spanish_shortcode' );
*/

/* Code Example
[spanish]Deme un momento por favor.[/spanish]
*/

/************* HIDING SPECIFIC PAGES FROM INTERNAL SEARCH *********************/

/*
function search_filter( $query ) {
	if ( $query->is_search && $query->is_main_query() ) {
		$query->set( 'post__not_in', array( 626,617 ) ); 
	}
}
add_filter( 'pre_get_posts', 'search_filter' );
*/

/************* THE EVENT CALENDAR FUNCTIONS *********************/

if( function_exists ('TribeEvents') ) {

/* Changing the default date/time for new events */

	// enter in condition text
	class Change_Tribe_Default_Event_Times {
		const TWELVEHOUR = 12;
		const TWENTYFOURHOUR = 24;
	
		protected $start = 9;
		protected $end = 11;
		protected $mode = self::TWELVEHOUR;
	
		public function __construct($start_hour, $end_hour) {
			$this->settings($start_hour, $end_hour);
			$this->add_filters();
		}
	
		protected function settings($start_hour, $end_hour) {
			$this->set_mode();
			$this->start = $this->safe_hour($start_hour);
			$this->end = $this->safe_hour($end_hour);
		}
	
		protected function add_filters() {
			add_filter('tribe_get_hour_options', array($this, 'change_default_time'), 10, 3);
			add_filter('tribe_get_meridian_options', array($this, 'change_default_meridian'), 10, 3);
		}
	
		protected function set_mode() {
			if (strstr(get_option('time_format', TribeDateUtils::TIMEFORMAT), 'H'))
				$this->mode = self::TWENTYFOURHOUR;
		}
	
		protected function safe_hour($hour) {
			$hour = absint($hour);
			if ($hour < 0) $hour = 0; 		if ($hour > 23) $hour = 23;
			return $hour;
		}
	
		public function change_default_time($hour, $date, $isStart) {
			if ('post-new.php' !== $GLOBALS['pagenow']) return $hour; // Only intervene if it's a new event
	
			if ($isStart) return $this->corrected_time($this->start);
			else return $this->corrected_time($this->end);
		}
	
		protected function corrected_time($hour) {
			if (self::TWENTYFOURHOUR === $this->mode) return $hour;
			if ($hour > 12) return $hour - 12;
			return $hour;
		}
	
		public function change_default_meridian($meridian, $date, $isStart) {
			if ('post-new.php' !== $GLOBALS['pagenow']) return $meridian; // Only intervene if it's a new event
	
			$meridian = 'am';
			if ($isStart && 12 <= $this->start) $meridian = 'pm';
			if (! $isStart && 12 <= $this->end) $meridian = 'pm';
	
			if (strstr(get_option('time_format', TribeDateUtils::TIMEFORMAT), 'A'))
				$meridian = strtoupper($meridian);
	
			return $meridian;
		}
	}

	new Change_Tribe_Default_Event_Times(12, 13);
	
	
	/* Sets is_404 to false on pages without any events like when the Upcoming Events page is empty. */
	function avoid_404_event_titles( $template ) {
		global $wp_query;
	
		if ( property_exists( $wp_query, 'tribe_is_event' ) && $wp_query->tribe_is_event && $wp_query->is_404 )
		$wp_query->is_404 = false;
	
		return $template;
	}
	add_action( 'template_include', 'avoid_404_event_titles', 1 );
	
	/* Show past events in reverse chronological order */
	function tribe_past_reverse_chronological ($post_object) {
		$past_ajax = (defined( 'DOING_AJAX' ) && DOING_AJAX && $_REQUEST['tribe_event_display'] === 'past') ? true : false;
		if(tribe_is_past() || $past_ajax) {
			$post_object = array_reverse($post_object);
		}
		return $post_object;
	}
	add_filter('the_posts', 'tribe_past_reverse_chronological', 100);
	
	/* Remove AJAX from events */
	function tribe_prevent_ajax_paging() {
		echo "
		<script>
		jQuery(document).ready(function(){
			// Tribe Bar
			jQuery( 'form#tribe-bar-form' ).off( 'submit' );
			jQuery( '#tribe-events' )
				.off( 'click', '.tribe-events-nav-previous, .tribe-events-nav-next' ) // Month and Week View
				.off( 'click', '.tribe-events-nav-previous a, .tribe-events-nav-next a') // Day View
				.off( 'click', '#tribe-events-footer .tribe-events-nav-previous a, #tribe-events-footer .tribe-events-nav-next a' ); // Day View
			// Month View
			jQuery( 'body' ).off( 'click', '#tribe-events-footer .tribe-events-nav-previous, #tribe-events-footer .tribe-events-nav-next' );
			// List View
			jQuery( '#tribe-events-content-wrapper' )
				.off( 'click', 'ul.tribe-events-sub-nav a[rel=\"next\"]' )
				.off( 'click', 'ul.tribe-events-sub-nav a[rel=\"prev\"]' )
				.off( 'click', '#tribe-events-footer .tribe-events-nav-previous a, #tribe-events-footer .tribe-events-nav-next a' );
			/* This breaks map and Photo view as of v4.0, because their nonJS href='#'
			// Map and Photo View
			jQuery( '#tribe-events' )
				.off( 'click', 'li.tribe-events-nav-next a')
				.off( 'click', 'li.tribe-events-nav-previous a');
			*/
		});
		</script>";
	}
	add_action( 'wp_footer', 'tribe_prevent_ajax_paging', 99 );
}

/************* MAKING IMAGE CAPTIONS IN HTML5 *********************/

add_filter( 'img_caption_shortcode', 'cleaner_caption', 10, 3 );

function cleaner_caption( $output, $attr, $content ) {

	/* We're not worried abut captions in feeds, so just return the output here. */
	if ( is_feed() )
		return $output;

	/* Set up the default arguments. */
	$defaults = array(
		'id' => '',
		'align' => 'alignnone',
		'width' => '',
		'caption' => ''
	);

	/* Merge the defaults with user input. */
	$attr = shortcode_atts( $defaults, $attr );

	/* If the width is less than 1 or there is no caption, return the content wrapped between the [caption]< tags. */
	if ( 1 > $attr['width'] || empty( $attr['caption'] ) )
		return $content;

	/* Set up the attributes for the caption <div>. */
	$attributes = ( !empty( $attr['id'] ) ? ' id="' . esc_attr( $attr['id'] ) . '"' : '' );
	$attributes .= ' class="wp-caption ' . esc_attr( $attr['align'] ) . '"';
	$attributes .= ' style="width: ' . esc_attr( $attr['width'] ) . 'px"';

	/* Open the caption <div>. */
	$output = '<figure' . $attributes .'>';

	/* Allow shortcodes for the content the caption was created for. */
	$output .= do_shortcode( $content );

	/* Append the caption text. */
	$output .= '<figcaption class="wp-caption-text">' . $attr['caption'] . '</figcaption>';

	/* Close the caption </div>. */
	$output .= '</figure>';

	/* Return the formatted, clean caption. */
	return $output;
}

/************* SETTING EXCERPT STRING *********************/

function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

/* DON'T DELETE THIS CLOSING TAG */ ?>