<?php
// Alumni Post Type Settings

// add custom categories
register_taxonomy( 'alumni_cat', 
	array('alumni'), /* if you change the name of register_post_type( 'alumni', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Alumni Categories', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Alumni Category', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Alumni Categories', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Alumni Categories', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Alumni Category', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Alumni Category:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Alumni Category', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Alumni Category', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Alumni Category', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Alumni Category Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_admin_column' => true, 
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'alumni' )
	)
);

// add custom tags
register_taxonomy( 'degrees', 
	array('alumni'), /* if you change the name of register_post_type( 'courses_type', then you have to change this */
	array('hierarchical' => false,    /* if this is false, it acts like tags */
		'labels' => array(
			'name' => __( 'Degrees', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Degrees', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Degrees', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Degrees', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Degrees', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Degrees:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Degrees', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Degrees', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Degrees', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Degrees Name', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_tagcloud' => false,
		'show_ui' => true,
		
		'query_var' => true,
	)
);

// add custom tags
register_taxonomy( 'class_year', 
	array('alumni'), /* if you change the name of register_post_type( 'courses_type', then you have to change this */
	array('hierarchical' => false,    /* if this is false, it acts like tags */
		'labels' => array(
			'name' => __( 'Year', 'bonestheme' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Class Year', 'bonestheme' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Class Year', 'bonestheme' ), /* search title for taxomony */
			'all_items' => __( 'All Class Year', 'bonestheme' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Class Year', 'bonestheme' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Class Year:', 'bonestheme' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Class Year', 'bonestheme' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Class Year', 'bonestheme' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Class Year', 'bonestheme' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Class Year', 'bonestheme' ) /* name title for taxonomy */
		),
		'show_ui' => true,
		'query_var' => true,
	)
);

// let's create the function for the custom type
function alumni_post_type() { 
	// creating (registering) the custom type 
	register_post_type( 'alumni', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Alumni', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Person', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Alumni', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Person', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Person', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Person', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Person', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Alumni', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'No alumni added yet.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Contains all members of the department', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 5, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-businessman', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'alumnus', 'with_front' => true ), /* you can specify its url slug */
			'capability_type' => 'post',
			'hierarchical' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'revisions', 'author' )
			
		) /* end of options */
	); /* end of register post type */
}

// adding the function to the Wordpress init
add_action( 'init', 'alumni_post_type');	
?>