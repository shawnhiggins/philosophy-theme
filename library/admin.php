<?php
/*
This file handles the admin area and functions. Use 
this file to make changes to anything administrative.
*/

/************* REMOVE POST METABOXES *****************/

// Removing Wordpress fields not needed.

function remove_meta_boxes() {
	remove_meta_box('postcustom', 'post', 'side');
	remove_meta_box('tagsdiv-areas_of_study', 'people', 'side' );
	remove_meta_box('tagsdiv-languages', 'people', 'side' );
}
add_action( 'admin_menu', 'remove_meta_boxes' );

/************* REMOVE DASHBOARD WIDGETS *****************/

// The items listed below are being disabled from the dashboard. Comment out if you want it to appear.

function disable_default_dashboard_widgets() {
	global $wp_meta_boxes;
	// disable default dashboard widgets
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);		// Right Now Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);			// Activity Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);	// Comments Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);	// Incoming Links Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);			// Plugins Widget
	// unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);		// Quick Press Widget
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);		// Recent Drafts Widget
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);			//

	// remove plugin dashboard boxes
	unset($wp_meta_boxes['dashboard']['normal']['core']['wpseo-dashboard-overview']);	// Yoast's SEO Plugin Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['tribe_dashboard_widget']);		// Modern Tribe Widget
}

/************* ADD DASHBOARD ITEMS *********************/

// Welcome message

function dashboard_info() { 
	echo '<p>This is the admin section of your website. To the left is a menu of different <strong>content types</strong> where you can create or edit content on your site. You may not see all of these options depending on your website and level of access.</p>
	<p>
		<ul>
			<li><strong>Posts</strong> manages your news section.</li>
			<li><strong>Pages</strong> is for most of the informational/static pages of your site.</li>
			<li><strong>Events</strong> is for managing events.</li>
			<li><strong>People</strong> is for all of the people pages of the department.</li>
			<li><strong>Courses</strong> is a listing of all courses being taught from semester to semester.</li>
			<li>Under <strong>Appearance</strong> you can edit the various <strong>Menus</strong> and <strong>Widgets</strong> throughout your website.</li>
			<li><strong>Users</strong> allows you to create and edit user accounts for your department.</li>
			<li><strong>Site Options</strong> is where you enter information about your department.</li>
		</ul>
	</p>
	<p>For questions, email <a href="mailto:webops@humnet.ucla.edu">webops@humnet.ucla.edu</a> or visit our <a href="https://spaces.ais.ucla.edu/display/cdhwebops">WebOps Wiki</a>, and for any techical issues related to Wordpress or your website, reach out to <a href="mailto:webopssupport@humnet.ucla.edu">webopssupport@humnet.ucla.edu</a>.</p>';
}

/*
// RSS Dashboard Widget Sample

function bones_rss_dashboard_widget() {
	if ( function_exists( 'fetch_feed' ) ) {
		// include_once( ABSPATH . WPINC . '/feed.php' );               // include the required file
		$feed = fetch_feed( 'http://feeds.feedburner.com/wpcandy' );    // specify the source feed
		if (is_wp_error($feed)) {
			$limit = 0;
			$items = 0;
		} else {
			$limit = $feed->get_item_quantity(7);                        // specify number of items
			$items = $feed->get_items(0, $limit);                        // create an array of items
		}
	}
	if ($limit == 0) echo '<div>The RSS Feed is either empty or unavailable.</div>';   // fallback message
	else foreach ($items as $item) { ?>

	<h4 style="margin-bottom: 0;">
		<a href="<?php echo $item->get_permalink(); ?>" title="<?php echo mysql2date( __( 'j F Y @ g:i a', 'bonestheme' ), $item->get_date( 'Y-m-d H:i:s' ) ); ?>" target="_blank">
			<?php echo $item->get_title(); ?>
		</a>
	</h4>
	<p style="margin-top: 0.5em;">
		<?php echo substr($item->get_description(), 0, 200); ?>
	</p>
	<?php }
}
*/
// calling all custom dashboard widgets
function bones_custom_dashboard_widgets() {
	wp_add_dashboard_widget('custom_help_widget', 'Welcome To Your Website', 'dashboard_info');
//	wp_add_dashboard_widget( 'bones_rss_dashboard_widget', __( 'Recently on Themble (Customize on admin.php)', 'bonestheme' ), 'bones_rss_dashboard_widget' );
	/*
	Be sure to drop any other created Dashboard Widgets
	in this function and they will all load.
	*/
}

// removing the dashboard widgets
add_action( 'wp_dashboard_setup', 'disable_default_dashboard_widgets' );

// Remove only if user is not an admin.
/*
if (!current_user_can('manage_options')) {
	add_action('wp_dashboard_setup', 'disable_default_dashboard_widgets');
}
*/

// adding any custom widgets
add_action( 'wp_dashboard_setup', 'bones_custom_dashboard_widgets' );


/************* CUSTOM LOGIN PAGE *****************/

// calling your own login css so you can style it

function bones_login_css() {
	wp_enqueue_style( 'bones_login_css', get_template_directory_uri() . '/library/css/login.css', false );
}

// changing the logo link from wordpress.org to your site
function bones_login_url() {  return home_url(); }

// changing the alt text on the logo to show your site name
function bones_login_title() { return get_option( 'blogname' ); }

// calling it only on the login page
add_action( 'login_enqueue_scripts', 'bones_login_css', 10 );
add_filter( 'login_headerurl', 'bones_login_url' );
add_filter( 'login_headertitle', 'bones_login_title' );

/************* CUSTOMIZE ADMIN *******************/

/*
I don't really recommend editing the admin too much
as things may get funky if WordPress updates. Here
are a few funtions which you can choose to use if
you like.
*/

// Custom Backend Footer
function bones_custom_admin_footer() {
	_e( '<span id="footer-thankyou">Developed by <a href="http://cdh.ucla.edu" target="_blank">Center for Digital Humanities</a></span>.', 'bonestheme' );
}

// adding it to the admin area
add_filter( 'admin_footer_text', 'bones_custom_admin_footer' );

/************* ORDERING ADMIN MENU *********************/

// Order the array below in the order you'd like sections in Wordpress

function custom_menu_order($menu_ord) {
	if (!$menu_ord) return true;
     
	return array(
		'index.php', // Dashboard
		'separator1', // First separator
		
		'edit.php', // Posts
		'edit.php?post_type=page', // Pages
		'edit.php?post_type=tribe_events', // Events
		'edit.php?post_type=people', // People
		'edit.php?post_type=books', // Books
		'edit.php?post_type=courses', // Courses
		'edit.php?post_type=conference', // Conference
		'edit.php?post_type=alumni', // Alumni
		'upload.php', // Media
		'separator2', // Second separator
		
		'themes.php', // Appearance
		'plugins.php', // Plugins
		'users.php', // Users
		'tools.php', // Tools
		'options-general.php', // Settings
		'separator-last', // Last separator
		
		'admin.php?page=site-options', // Site Options
    );
}
add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order');

/************* REMOVING ADMIN MENU ITEMS *********************/

// Removing comments because this is almost never needed

function remove_admin_menu_items() {
	$remove_menu_items = array(__('Comments'));
	global $menu;
	end ($menu);
	while (prev($menu)){
		$item = explode(' ',$menu[key($menu)][0]);
		if(in_array($item[0] != NULL?$item[0]:"" , $remove_menu_items)){
			unset($menu[key($menu)]);
		}
	}
}
add_action('admin_menu', 'remove_admin_menu_items');

/************* TINY MCE EDITOR *********************/

// Removing buttons I don't want content managers to use

function myplugin_tinymce_buttons($buttons) {
	// Remove the text color selector
	$remove = 'forecolor';
	
	// Find the array key and then unset
	if ( ( $key = array_search($remove,$buttons) ) !== false )
	unset($buttons[$key]);
	
	return $buttons;
}
add_filter('mce_buttons_2','myplugin_tinymce_buttons');

// Adding style select dropdown

function wpb_mce_buttons_2($buttons) {
	array_unshift($buttons, 'styleselect');
	return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

// Populating style select dropdown
// http://www.wpbeginner.com/wp-tutorials/how-to-add-custom-styles-to-wordpress-visual-editor/

function my_mce_before_init_insert_formats( $init_array ) {  

// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'Subtitle',
			'selector' => 'p',
			'classes' => 'content-subtitle',
		),
		array(  
			'title' => 'Warning Bubble',
			'selector' => 'p',
			'classes' => 'alert warning',
		),
		array(  
			'title' => 'Info Bubble',
			'selector' => 'p',
			'classes' => 'alert info',
		),
        array(  
			'title' => 'Link Button',
			'selector' => 'a',
			'classes' => 'btn',
		),
        array(  
			'title' => 'Stylish Blockquote',
			'selector' => 'blockquote',
			'classes' => 'stylish',
		),
        array(  
			'title' => 'Two Column Paragraph',
			'selector' => 'p',
			'classes' => 'two-col',
		),
        
        array(  
			'title' => 'Two Column Paragraph with border',
			'selector' => 'p',
			'classes' => 'two-col with-border',
		),
        
        array(  
			'title' => 'Two Column Div',
			'selector' => 'div',
			'classes' => 'two-col',
		),
        
        array(  
			'title' => 'Two Column Div with border',
			'selector' => 'div',
			'classes' => 'two-col with-border',
		),
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

/************* THEME OPTIONS *********************/

// Using Advanced Custom Fields plugin

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Site Options',
		'menu_title'	=> 'Site Options',
		'menu_slug' 	=> 'site-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Features',
		'menu_title'	=> 'Features',
		'parent_slug'	=> 'site-options',
	));
}
?>