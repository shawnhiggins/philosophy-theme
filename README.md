# _SOL #
#### Version 1.8 ####
_SOL is a Wordpress starter theme developed for use with academic websites, based on the [Bones](https://github.com/eddiemachado/bones) starter theme. It comes with some common functionality academic websites might find useful, and has some general styling, but it is meant to be used as a starting point for you to customized as needed for new projects.

## Getting Started ##
I recommend taking the following steps when using this theme:

1. [Download](https://bitbucket.org/tuckerl/_sol-theme/downloads) the theme from BitBucket.
2. [Install the theme](https://codex.wordpress.org/Using_Themes) in Wordpress.
3. Log in to Wordpress and follow the prompts to install the recommended and required plugins.
4. [Download](https://ucla.box.com/sol-custom-fields) and upload the custom fields for the Advanced Custom Fields plugin. This step is **important**.
5. Within Wordpress, go to Site Options > Features and enable the features you'd like to use (such as Books, Courses, People, and Conferences).
6. [Download](#markdown-header-downloads) and upload the placeholder content.
7. [Download](#markdown-header-setting-recommendations) and upload settings for each plugin being used.
8. Customize as you see fit!

## Latest Major Changes ##
* Added info bubbles as WYSIWYG options.
* Come with conference template.
* Homepage columns are now more flexible.
* Cleaned up code.
* Edited recommended plugins.

## Plugins ##
_SOL automatically recommends and makes use of the plugins below. Optional plugins are useful depending on the website.

#### Required ####
* [Advanced Custom Fields Pro](https://wordpress.org/plugins/advanced-custom-fields/) - (Pro version not free) Heavily used for creating custom fields and controlling theme functions.
* [All In One WP Security & Firewall](https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/) - A comprehensive, user-friendly, all in one WordPress security and firewall plugin for your site.

#### Recommended ####
* [Advanced Access Manager](https://wordpress.org/plugins/advanced-access-manager/) - Manager user roles for Wordpress backend.
* [Gravity Forms](http://www.gravityforms.com/) - (Not free) For creating web forms.
* [Imsanity](https://wordpress.org/plugins/imsanity/) - Prevents and resizes large image uploads.
* [Regenerate Thumbnails](https://wordpress.org/plugins/regenerate-thumbnails/) - Allows you to regenerate all thumbnails after changing the thumbnail sizes.
* [Redirection](https://wordpress.org/plugins/redirection/) - Manage 301 redirects and monitor 404 errors.
* [The Events Calendar](https://wordpress.org/plugins/the-events-calendar/) - Integrated into the theme for events.
* [Yoast SEO](https://wordpress.org/plugins/wordpress-seo/) - Best way to manage SEO for Wordpress.

#### Optional ####
* [Broken Link Checker](https://wordpress.org/plugins/broken-link-checker/) - Checks site for broken links and missing images.
* [Contact Form 7](https://wordpress.org/plugins/contact-form-7/) - Simple contact form plugin.
* [Post Types Order](https://wordpress.org/plugins/post-types-order/) - Allow admin to drag and drop to reorder post.
* [Responsive Lightbox](https://wordpress.org/plugins/responsive-lightbox/) - Use with image galleries.
* [TablePress](https://wordpress.org/plugins/tablepress/) - Nice way to manage tabular data.

## jQuery Plugins ##
The following jQuery plugins are included in the theme:

* [Accessible Menu](https://github.com/adobe-accessibility/Accessible-Mega-Menu) - Makes main menu controllable by keyboard.
* [Isotope](http://isotope.metafizzy.co) - For filtering and sorting layouts via AJAX.
* [BxSlider](http://bxslider.com) - Responsive content slider.
* [Modernizr](http://modernizr.com) - Allows you to take advantage of HTML5 and CSS3 features by detecting it in the user’s browser.
* [SlickNav Responsive Mobile Menu](http://slicknav.com/) - Functionality for mobile nav.
* [Sticky-kit](https://github.com/leafo/sticky-kit/) - A jQuery plugin for creating smart sticky elements.

## SASS ##
This theme is built using [SASS](http://sass-lang.com/) to write CSS. If you don't want to use SASS, simply ignore the scss directory and instead use the css directory to edit the site's styling. If you do want to take advantage of SASS, you'll need a way to compile your SASS into CSS files. There are [many tools](http://mashable.com/2013/06/11/sass-compass-tools/), but if you are using a Mac I recommend [Prepros](http://alphapixels.com/prepros/).

## Downloads ##
Aside from the required plugins, you might also want to download these files to help you get started with your website. Settings should be uploaded within that particular plugin's interface.

### Content Downloads ###
* [Books](https://ucla.box.com/sol-books) - Fake books to use for demo and development purposes.
* [Courses](https://ucla.box.com/sol-courses) - Fake courses to use for demo and development purposes.
* [Pages](https://ucla.box.com/sol-pages) - Common pages that might be needed on an academic website with placeholder content.
* [People](https://ucla.box.com/sol-people) - Fake people to use for demo and development purposes. 
* [Posts](https://ucla.box.com/sol-posts) - Fake posts to use for demo and development purposes.
* [Conferences](https://ucla.box.com/v/sol-conferences) - Fake conference pages to use for demo and development purposes.

### Plugin Downloads ###
* [Custom Fields](https://ucla.box.com/sol-custom-fields) - These are the custom fields the theme uses. Import these to Advanced Custom Fields or the theme will not work properly.
* [Gravity Forms](https://ucla.box.com/gravity-forms-example) - Forms you can import into your site to use.
* [SEO Settings](https://ucla.box.com/seo-settings) - Settings for Yoast SEO plugin.
* [WP Security Settings](https://ucla.box.com/security-settings) - Settings for All In One WP Security & Firewall plugin.

## Setting Recommendations ##
* **General Settings** - Under Settings > General, be sure to set a timezone. At Settings > Reading, set the Front Page to Home (available in the [Pages](https://ucla.box.com/sol-pages) download). If you do not, the homepage hero image will pull from the most recent post.
* **Gravity Forms** - When using Gravity Forms, edit the Settings and set Output CSS to "No" and Output HTML5 to "Yes."
* **Yoast SEO** - _SOL integrates with Yoast SEO to create breadcrumbs. When installed, go to SEO > Advanced and check "Enable Breadcrumbs."
* **The Events Calendar** - From within the Events section, go to Settings > Display and select Full Styles to get a slight change in design that better matches the theme.