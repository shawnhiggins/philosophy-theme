<?php get_header();
session_start();
?>

<?php

$terms = get_the_terms( $post->ID , 'quarterly');
if($terms) {
	foreach( $terms as $term ) {
		$cat_obj = get_term($term->term_id, 'quarterly');
		$cat_slug = $cat_obj->slug;
	}
}
//echo 'The slug is '. $cat_slug;
?>

<div class="content">
    <div class="col" id="main-content" role="main">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
            
            <?php if ( has_post_thumbnail() ) {
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'content-width' );
		$url = $thumb['0']; ?>
        <div id="hero" style="background-image: url('<?=$url?>');">
            <div>
               <div class="phil-logo"></div>
                <div class="colloquy-logo"> 
                    <div class="newsletter-issue">
                        <?php                                                                                             
                            $quarterly_terms = wp_get_object_terms($post->ID, 'quarterly');
                            if ( ! empty( $quarterly_terms ) ) {
                                if ( ! is_wp_error( $quarterly_terms ) ) {
                                        foreach( $quarterly_terms as $term ) {
                                            echo '<span class="cat-title">' . esc_html( $term->name ) . '</span>'; 
                                            break;
                                        }
                                    }
                                }
                            ?>
                    
                    </div>
                </div>
            </div>
        </div>
        <?php } else { ?>
            <?php get_template_part('snippets/newsletter', 'header'); ?>            
             
            <?php if((get_field('page_type') !== "interview-blog") ) { ?>
                <h1 class="entry-title single-title" itemprop="headline"><?php if(get_field('page_type') == "interview") { echo "Interview with "; } ?><?php the_title(); ?></h1>
            <?php } } ?>
            <section class="entry-content cf" itemprop="articleBody">
                <?php the_content(); ?>
                <?php $i = 0; 
                if(get_field('select_pages')){                    
                    get_template_part('snippets/newsletter', 'parent');
                }
                    // This is to display Single Page Interviews
                    if(get_field('page_type') == "interview") {                        
                       // if( have_rows('interview') ) {
                       // while ( have_rows('interview') ) { the_row();                                                          
                       get_template_part('snippets/newsletter', 'interview');
                                                       //  } }
                    } 
                    // This will display UnderGraduate Q and A
                    if(get_field('page_type') == "undergrad-qa") {
                        
                        if( have_rows('philosophers_q_a') ) {
                        while ( have_rows('philosophers_q_a') ) { the_row();
                                                                 
                            get_template_part('snippets/newsletter', 'qa');
                            }
                        }
                    } 
                    /* THIS WILL DISPLAY IN MEMORIAM */
                    if(get_field('page_type') == "memoriam") {
                        
                        if( have_rows('memoriam') ) {
                            while ( have_rows('memoriam') ) { the_row();
                        
                        get_template_part('snippets/newsletter', 'memoriam');
                                                        }
                        }
                    } 
                    /* THIS WILL DISPLAY ARRIVALS AND DEPARTURES */
                    if(get_field('page_type') == "arrivals") {
                        if( have_rows('arrivals_departures') ) {
                            while ( have_rows('arrivals_departures') ) { the_row();

                        get_template_part('snippets/newsletter', 'arrivals');
                                                                       } }
                    } 
                    /* THIS WILL DISPLAY AWARDS */
                    if(get_field('page_type') == "awards") {
                        if( have_rows('awards') ) {
                        while ( have_rows('awards') ) { the_row();
                                                       
                        get_template_part('snippets/newsletter', 'awards');
                                                      }
                        }
                    } 
                    /* THIS WILL DISPLAY RECENT BOOKS BLOG */
                    if(get_field('page_type') == "recent-books") {
                        get_template_part('snippets/newsletter', 'books');
                    } 
                    /* THIS WILL DISPLAY INTERVIEW BLOG */
                    if(get_field('page_type') == "interview-blog") {
                        
                        $_SESSION['page_type'] = get_field('page_type');
                        //$term = get_sub_field('category');
                        //$amount = get_sub_field('amount_to_show');
                        $term = 'interviews';
                        $amount = 10;
                         
                        $args = array(
                            'quarterly' => $cat_slug, 
                            'post_type' => 'newsletter', 
                            'meta_query' 		=> array(
                                array(
                                    'key'			=> 'page_type',
                                    'value'			=> 'interview'
                                )
                            ),
                            'order'				=> 'ASC',
                            'orderby'			=> 'menu_order',
                        );
                         $posts_query = new WP_Query( $args );
                       //$posts_query = new WP_Query( array( 'quarterly' => 'interviews', 'post_type' => 'newsletter', 'orderby' => 'menu_order', 'order' => 'ASC') );                    
                    ?>
                    <div class="interviews">
                        <ul class="blog">
                            <?php if ($posts_query->have_posts()) : while ($posts_query->have_posts()) : $posts_query->the_post(); ?>

                                <li>

                                    <section class="interview">
                                        <a href="<?php the_permalink() ?>">
                                            <h1>Interview with <?php the_title(); ?></h1>
                                        </a>
                                        <?php                                   

                            get_template_part('snippets/newsletter', 'interview');
                            ?>

                                    <a href="<?php the_permalink() ?>">READ MORE ></a>

                                    </section>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                <?php } ?>                

            </section>
        </article>

    <?php endwhile; else : ?>

        <article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
            <h1>Page Not Found</h1>
            <section>
                <p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
            </section>
        </article>

    <?php endif; ?>

    </div>
    
    <div class="col side">
        <div class="content">
            <nav class="page-nav" role="navigation" aria-labelledby="section navigation">
                <h3>In The Issue</h3>
                    <?php if ( $post->post_parent > 0 ) {
                            $parent = array_pop( get_post_ancestors($post->ID) );
                        }
                        else {
                            $parent = $post->post_parent;
                        } 
                    ?>
                <ul>
                    <li><a href="<?php echo get_permalink($parent); ?>">Home</a></li>
                    <?php
                    if(!$post->post_parent) $my_page_id = $post->ID;
                    elseif($post->ancestors) $my_page_id = end($post->ancestors);

                    $list_p = array(
                     'sort_column' => 'menu_order, post_title',
                    'post_type' => 'newsletter',
                     'title_li' => '',
                     'child_of' => $my_page_id,
                    );
                    wp_list_pages( $list_p );
                    ?>
                </ul>                
            </nav>
        </div>
    </div>
</div>
        

<?php get_footer(); ?>
