<?php get_header(); ?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<header class="bio <?php if(get_field('person_type') == "alumni") { ?>alumni<?php } ?>">
					<div class="content">
						<?php if(get_field('photo')) {
							$image = get_field('photo');
							if( !empty($image) ): 
							// vars
							$url = $image['url'];
							$title = $image['title'];
							// thumbnail
							$size = 'people-large';
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ];
						endif; ?>
						<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
						<?php } else { ?>
						<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-people-logo-220.jpg" alt="Silhouette" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
						<?php } ?>
						<section>
							<h1 id="bio"><?php the_title(); ?></h1>
							<?php if(get_field('position_title')) { ?>
							<span class="position"><?php the_field('position_title'); ?></span>
							<?php } ?>
							<?php if(get_field('person_type') == "alumni") { ?>
							<div class="employment">
								<?php if(get_field('position_title')) { ?>
								<span class="position"><?php the_field('position_title'); ?></span>
								<?php } ?>
								<?php if(get_field('employer')) { ?><span class="employer"> at <?php the_field('employer'); ?></span>
								<?php } ?>
							</div>
							<?php } ?>
                            
                            
                            <?php if(get_field('interest')) { ?>
                                <div class="interest">
                                   <strong>Area of Interest: </strong> <?php the_field('interest'); ?>
                                </div>
                            <?php } ?>       
							<div class="details">
							<?php if(get_field('email_address')) { ?>
								<span><strong>E-mail: </strong><a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a></span>
							<?php } ?>
							<?php if(get_field('phone_number')) { ?>
								<span><strong>Phone: </strong><?php the_field('phone_number'); ?></span>
							<?php } ?>
							<?php if(get_field('office')) { ?>
								<span><strong>Office: </strong><?php the_field('office'); ?></span>
							<?php } ?>
							<?php if(get_field('office_hours')) { ?>
								<p><strong>Office Hours: </strong><?php the_field('office_hours'); ?></p>
							<?php } ?>
							</div>
							<?php the_content(); ?>
						</section>
					</div>
				</header>
				<div class="content main">
					<div class="col" id="main-content" role="main">
						<?php if(get_field('education')) { ?>
						<section id="education">
							<h2>Education</h2>
							<?php the_field('education'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('research')) { ?>
						<section id="research">
							<h2>Research</h2>
							<?php the_field('research'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('books') || get_field('articles') || get_field('publications') || get_field('additional_works') || get_field('dissertations')) { ?>
						<section id="publications">
							<h2>Publications</h2>
							<?php if(get_field('books')) { ?>
							<section id="books">
								<h3>Books</h3>
								<?php $book = get_field('books'); ?>
								<ul class="book-list">
									<? if( $book ): ?>
									<?php foreach( $book as $post): ?>
									<?php setup_postdata($post); ?>
									<li>
										<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
										<?php if(get_field('book_cover')) {
											$image = get_field('book_cover');
											if( !empty($image) ): 
												// vars
												$url = $image['url'];
												$title = $image['title'];
												// thumbnail
												$size = 'small-book';
												$thumb = $image['sizes'][ $size ];
												$width = $image['sizes'][ $size . '-width' ];
												$height = $image['sizes'][ $size . '-height' ];
											endif; ?>
											<img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> book cover" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="cover" />
											<?php } else { ?>
												<div class="custom-cover cover">
													<span class="title"><?php the_title(); ?></span>
												</div>
											<?php } ?>
										</a>
										<dl>
											<dt class="title">
												<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
											</dt>
											<?php if(get_field('subtitle')) { ?>
											<dd class="subtitle">
												<?php the_field('subtitle'); ?>
											</dd>
											<?php } ?>
											<?php if(get_field('publisher')) { ?>
											<dd class="publisher">
												<?php the_field('publisher'); ?>, <?php the_field('published_date'); ?>
											</dd>
											<?php } ?>
										</dl>
									</li>
									<?php endforeach; ?>
									<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</ul>
							</section>
							<?php } ?>
							<?php if(get_field('articles')) { ?>
							<section id="articles">
								<h3>Articles</h3>
							<?php the_field('articles'); ?>
							</section>
							<?php } ?>
							<?php if(get_field('publications')) { ?>
							<section id="additional_publications">
							<?php the_field('publications'); ?>
							</section>
							<?php } ?>
							<?php if(get_field('dissertations')) { ?>
							<section id="dissertations">
								<h3>Dissertations</h3>
								<?php the_field('dissertations'); ?>
							</section>
							<?php } ?>
							<?php if(get_field('additional_works')) { ?>
							<section id="works">
								<?php the_field('additional_works'); ?>
							</section>
							<?php } ?>
						</section>
						<?php } ?>
						<?php if(get_field('honors_and_awards')) { ?>
						<section id="honors_and_awards">
							<h2>Honors and Awards</h2>
							<?php the_field('honors_and_awards'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('positions_held')) { ?>
						<section id="positions_held">
							<h2>Positions Held</h2>
							<?php the_field('positions_held'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('courses')) { ?>
						<section id="courses">
							<h2>Courses</h2>
							<?php the_field('courses'); ?>
						</section>
						<?php } ?>
						<?php if(get_field('custom_section_title')) { ?>
						<section id="other">
							<h2><?php the_field('custom_section_title'); ?></h2>
							<?php the_field('custom_section_content'); ?>
						</section>
						<?php } ?>
					</div>
					<?php 
					// Make nav appear if only if there is anything to show
					if(get_field('education') || get_field('research') || get_field('books') || get_field('articles') || get_field('additional_works') || get_field('dissertations') || get_field('positions_held') || get_field('honors_and_awards') || get_field('courses') || get_field('custom_section_title') || get_field('cv') || get_field('personal_website') || get_field('academia_profile') || get_field('additional_link')) { ?>
					<div class="col person-nav nav-container">
						<div class="content col-nav">
							<nav role="navigation" aria-labelledby="person navigation">
								<ul class="table-of-contents">
									<h3>About <?php the_field('first_name'); ?></h3>
									<?php if( empty( $post->post_content) ) {
									// If there is no bio, don't show bio link
									} else { ?>
									<li><a href="#bio">Bio</a></li>
									<?php } ?>
									<?php if(get_field('education')) { ?>
									<li><a href="#education">Education</a></li>
									<?php } ?>
									<?php if(get_field('research')) { ?>
									<li><a href="#research">Research</a></li>
									<?php } ?>
									<?php if(get_field('books') || get_field('articles') || get_field('additional_works') || get_field('dissertations')) { ?>
									<li><a href="#publications">Publications</a></li>
									<?php } ?>
									<?php if(get_field('honors_and_awards')) { ?>
									<li><a href="#honors_and_awards">Honors & Awards</a></li>
									<?php } ?>
									<?php if(get_field('positions_held')) { ?>
									<li><a href="#positions_helds">Positions Held</a></li>
									<?php } ?>
									<?php if(get_field('courses')) { ?>
									<li><a href="#courses">Courses</a></li>
									<?php } ?>
									<?php if(get_field('custom_section_title')) { ?>
									<li><a href="#other"><?php the_field('custom_section_title'); ?></a></li>
									<?php } ?>
									<?php if(get_field('cv')) { ?>
									<li><a href="<?php the_field('cv'); ?>" class="download">Download CV</a></li>
									<?php } ?>
									<?php if(get_field('personal_website')) { ?>
									<li><a href="<?php the_field('personal_website'); ?>" class="link">Personal Website</a></li>
									<?php } ?>
									<?php if(get_field('academia_profile')) { ?>
									<li><a href="<?php the_field('academia_profile'); ?>" class="link">Academia Profile</a></li>
									<?php } ?>
									<?php if(get_field('additional_link')) { ?>
									<li><a href="<?php the_field('additional_link'); ?>" class="link"><?php the_field('additional_link_title'); ?></a></li>
									<?php } ?>
								</ul>
							</nav>
						</div>
					</div>
					<?php } ?>
				</div>
			<?php endwhile; ?>
			<?php else : endif; ?>
<?php get_footer(); ?>