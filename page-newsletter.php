<?php
/*
 Template Name: Newsletter Cover Page Listing
*/
?>
<?php get_header(); ?>
<div class="content">
    <div class="col" id="main-content" role="main">
        <header>
            <h1><?php the_title(); ?></h1>

            <?php the_content(); ?>
        </header>
        <section class="newsletter">
            <?php if(get_field('newsletter_covers')) { ?>
                    <?php $cover = get_field('newsletter_covers'); ?>
                    <ul class="cover-list">
                        <? if( $cover ): ?>
                        <?php foreach( $cover as $post): ?>
                        <?php setup_postdata($post); ?>
                        <li>
                            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                            <?php if(get_field('newsletter_cover_image')) {
                                $image = get_field('newsletter_cover_image');
                                if( !empty($image) ): 
                                    // vars
                                    $url = $image['url'];
                                    $title = $image['title'];
                                    // thumbnail
                                    $size = 'newsletter-people-photo';
                                    $thumb = $image['sizes'][ $size ];
                                    $width = $image['sizes'][ $size . '-width' ];
                                    $height = $image['sizes'][ $size . '-height' ];
                                endif; ?>
                                <img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> newsletter cover" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="cover wp-post-image" />
                                <?php } else { ?>
                                    <div class="custom-cover cover">
                                        <span class="title"><?php the_title(); ?></span>
                                    </div>
                                <?php } ?>
                            </a>
                            <dl>
                                <dt class="title">
                                    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                                        <?php the_title(); ?>

                                        <?php if(get_field('subtitle')) { ?>
                                        <span class="subtitle">
                                        : <?php the_field('subtitle'); ?>
                                        </span>
                                        <?php } ?>
                                    </a>
                                </dt>
                            </dl>
                        </li>
                        <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                        <?php endif; ?>
                    </ul>
            <?php } ?>
        </section>
    </div>
        <?php get_sidebar(); ?>
</div>
                
<?php get_footer(); ?>