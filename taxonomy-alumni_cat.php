<?php get_header(); ?>
			<div class="content main" id="main-content">
				<header>
					<h1><?php single_cat_title(); ?></h1>
					<?php $category_description = category_description();
					if ( ! empty( $category_description ) )
					echo apply_filters( 'category_archive_meta', '<p>' . $category_description . '</p>' );
					?>
					<?php if ( has_nav_menu( 'faculty-filter2' ) ) {?> 
					<div class="filter">
					<?php // To make another filter, duplicate the div below ?>
						<div class="options button-group" data-filter-group="field">
							<h3>Field of Study</h3>
							<ul>
								<button data-filter="" data-text="All" class="option all is-checked">View All</button>
								<?php wp_nav_menu(array(
									'container' => false,
									'menu' => __( 'Faculty Filter', 'bonestheme' ),
									'menu_class' => 'faculty-filter',
									'theme_location' => 'faculty-filter',
									'before' => '',
									'after' => '',
									'depth' => 1,
									'items_wrap' => '%3$s',
									'walker' => new Filter_Walker
								)); ?>
							</ul>
						</div>
					</div>
					<h2 class="filter-title">All</h2>
					<?php } ?> 
				</header>
                
                <section class="alumni">
                    <div class="people-list">
                        <ul <?php post_class('cf'); ?>>
                            <?php   // vars
                                    $queried_object = get_queried_object(); 
                                    $taxonomy = $queried_object->taxonomy;
                                    $term_slug = $queried_object->slug;
                            ?>
                            <!--?php $core_loop = new WP_Query( array( $taxonomy => $term_slug, 'post_type' => 'alumni', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => array('class_year, cohort_year'), 'order' => 'DESC')); ?-->
                            
                            <?php 
                                $args =  array( 
                                    $taxonomy => $term_slug, 
                                    'post_type' => 'alumni', 
                                    'posts_per_page' => -1, 
                                    'orderby' => 'meta_value', 
                                    'meta_key' => 'class_year', 
                                    'order' => 'DESC'
                                );

                                $core_loop = new WP_Query( $args ); ?>
                            
                            
                            <?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
                                <li class="person-item<?php $areas = get_field('area_of_study'); if( $areas ): foreach( $areas as $area ): ?> <?php echo $area->slug; ?><?php endforeach; endif;?><?php $languages = get_field('language_of_study'); if( $languages ): foreach( $languages as $language ): ?> <?php echo $language->slug; ?><?php endforeach; endif;?>">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php // if there is a photo, use it
                                        if(get_field('photo')) {
                                            $image = get_field('photo');
                                            if( !empty($image) ): 
                                                // vars
                                                $url = $image['url'];
                                                $title = $image['title'];
                                                // thumbnail
                                                $size = 'people-thumb';
                                                $thumb = $image['sizes'][ $size ];
                                                $width = $image['sizes'][ $size . '-width' ];
                                                $height = $image['sizes'][ $size . '-height' ];
                                        endif; ?>
                                        <img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
                                        <?php // otherwise use a silhouette 
                                        } else { ?>
                                        <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-people-logo-220.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
                                        <?php } ?>
                                        <dl>
                                            <dt class="name"><?php the_title(); ?></dt>
                                            <?php if(get_field('class_year')) { ?>
                                            <dd class="year"> 
                                                    Class of <?php echo get_field('class_year'); ?>
                                            <?php } ?>
                                            <?php if(get_field('cohort_year')) { ?> 
                                                <br />
                                                    Cohort of <?php echo get_field('cohort_year'); ?>
                                            </dd>
                                            <?php } ?>
                                            <?php if(get_field('degree')) { ?>
                                                <dd class="degree"> 
                                                    <?php                                                                          
                                                        $field = get_field_object('degree');
                                                        $value = get_field('degree');
                                                        $label = $field['choices'][ $value ];
                                                    ?>
                                                   <strong>Degree: </strong> <?php echo $label; ?>
                                                </dd>
                                            <?php } ?>
                                            <?php if(get_field('current_occupation')) { ?>
                                                <dd class="occupation">
                                                   <strong>Occupation: </strong> <?php the_field('current_occupation'); ?>
                                                </dd>
                                            <?php } ?>
                                            
                                            <?php if (get_field('contact_me') == "yes"): ?>
                                                <?php if(get_field('phone_number')) { ?>
                                                <dd class="phone">
                                                    <strong>Phone: </strong><?php the_field('phone_number'); ?>
                                                </dd>
                                                <?php } ?>	
                                                <?php if(get_field('email_address')) { ?>
                                                <dd class="email">
                                                    <a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a>
                                                </dd>
                                                <?php } ?>
                                            <?php endif; ?>
                                        </dl>
                                    </a>
                                </li>
                            <?php endwhile; ?>					
                            </ul>  
                        </div>                                      
                    </section>                       
			</div>
<?php get_footer(); ?>