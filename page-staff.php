<?php
/*
 Template Name: Staff Page
*/
?>
<?php get_header(); ?>
			<div class="content main" id="main-content">
				<header>
					<h1><?php the_title(); ?></h1>
				</header>
				<div class="profile-list">
				    <?php if( have_rows('staff_profile') ): ?>                                        
					   <ul>
						<?php while( have_rows('staff_profile') ): the_row(); ?>
							<?php
                                $first_name = get_sub_field('first_name');
                                $last_name = get_sub_field('last_name');
								$person_name = $first_name. ' ' . $last_name;
                                $title = get_sub_field('title');
                                $email_address = get_sub_field('email_address');
                                $position_title = get_sub_field('position_title');
                                $phone_number = get_sub_field('phone_number');
                                $office = get_sub_field('office');
                                $office_hours = get_sub_field('office_hours');
								$personal_site = get_sub_field('personal_website');
								$image = get_sub_field('photo');
								if( !empty($image) ): 
									// vars
									$url = $image['url'];
									$title = $image['title'];
									// thumbnail
									$size = 'people-thumb';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];
								endif;
							?>		
							<?php if( $personal_site ): ?>
							<a href="<?php echo $personal_site; ?>" class="hero-link">
							<?php endif; ?>
                                <?php if($title){ ?>
                                    <h2><?php echo $title; ?></h2>
                                <?php } ?>
                                
                                
                                
                                
                                <?php $book = get_field('person'); ?>
								<ul class="book-list">
									<? if( $book ): ?>
									<?php foreach( $book as $post): ?>
									<?php setup_postdata($post); ?>
									<li>
										<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
										<?php if(get_field('photo')) {
											$image = get_field('photo');
											if( !empty($image) ): 
												// vars
												$url = $image['url'];
												$title = $image['title'];
												// thumbnail
												$size = 'people-thumb';
												$thumb = $image['sizes'][ $size ];
												$width = $image['sizes'][ $size . '-width' ];
												$height = $image['sizes'][ $size . '-height' ];
											endif; ?>
											<img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> book cover" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="cover" />
											<?php } else { ?>
												<div class="custom-cover cover">
													<span class="title"><?php the_title(); ?></span>
												</div>
											<?php } ?>
										</a>
										<dl>
											<dt class="title">
												<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
											</dt>
											<?php if(get_field('subtitle')) { ?>
											<dd class="subtitle">
												<?php the_field('subtitle'); ?>
											</dd>
											<?php } ?>
											<?php if(get_field('publisher')) { ?>
											<dd class="publisher">
												<?php the_field('publisher'); ?>, <?php the_field('published_date'); ?>
											</dd>
											<?php } ?>
										</dl>
									</li>
									<?php endforeach; ?>
									<?php wp_reset_postdata(); ?>
									<?php endif; ?>
								</ul>
                                
                                
                                
                                
                                
                                
                                
                                <li class="person-item<?php $areas = get_field('area_of_study'); if( $areas ): foreach( $areas as $area ): ?> <?php echo $area->slug; ?><?php endforeach; endif;?><?php $languages = get_field('language_of_study'); if( $languages ): foreach( $languages as $language ): ?> <?php echo $language->slug; ?><?php endforeach; endif;?>">
                                    <a href="<?php the_permalink() ?>">
                                        <?php // if there is a photo, use it
                                        if(get_field('photo')) {
                                            $image = get_field('photo');
                                            if( !empty($image) ): 
                                                // vars
                                                $url = $image['url'];
                                                $title = $image['title'];
                                                // thumbnail
                                                $size = 'people-thumb';
                                                $thumb = $image['sizes'][ $size ];
                                                $width = $image['sizes'][ $size . '-width' ];
                                                $height = $image['sizes'][ $size . '-height' ];
                                        endif; ?>
                                        <img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
                                        <?php // otherwise use a silhouette 
                                        } else { ?>
                                        <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-people-logo-220.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
                                        <?php } ?>
                                        <dl>
                                            <dt class="name"><?php the_title(); ?></dt>
                                            <dd class="position"><?php the_field('position_title'); ?></dd>
                                            <?php if(get_field('interest')) { ?>
                                            <dd class="interest">
                                                <?php the_field('interest'); ?>
                                            </dd>
                                            <?php } ?>                                    
                                            <?php if(get_field('phone_number')) { ?>
                                            <dd class="phone">
                                                <strong>Phone: </strong><?php get_sub_field('phone_number'); ?>x
                                            </dd>
                                            <?php } ?>	
                                            <?php if(get_field('email_address')) { ?>
                                            <dd class="email">
                                                <a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a>
                                            </dd>
                                            <?php } ?>	
                                        </dl>
                                    </a>
                                </li>
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
							<li class="person-item">
                                <?php // if there is a photo, use it
								if($image) { ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette 
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-people-logo-220.jpg" alt="A photo of <?php the_title(); ?>" class="photo default-img <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
									<dl>
                                        <dt class="name"><?php echo $person_name; ?></dt>
                                        <dd class="position"><?php echo $position_title; ?></dd>
                                        <?php if($email_address) { ?>
                                        <dd class="email">
                                            <a href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a>
                                        </dd>
                                        <?php } ?>
                                        <?php if($phone_number) { ?>
                                        <dd class="phone">
                                            <?php echo $phone_number; ?>
                                        </dd>
                                        <?php } ?> 
                                        <?php if($office) { ?>
                                        <dd class="office">
                                           <?php echo $office; ?>
                                        </dd>
                                        <?php } ?>
                                        <?php if($office_hours) { ?>
                                        <dd class="office">
                                            <?php echo $office_hours; ?>
                                        </dd>
                                        <?php } ?>
								    </dl>
                                    <?php if($personal_site) { ?>                                        
                                        <a class="btn" href="<?php echo $personal_site; ?>" target="_blank">Website</a>
                                    <?php } ?>
							</li>
							<?php if( $personal_site ): ?>
							</a>
							<?php endif; ?>
							<?php endwhile; ?>
                            </ul>
				        <?php endif; ?>
                                
					</div>
			</div>
<?php get_footer(); ?>