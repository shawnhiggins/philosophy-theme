<?php get_header(); ?>
			<div id="main-content" role="main">
			<?php query_posts( 'posts_per_page=1' ); ?>
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); 
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'home-hero' );
				$url = $thumb['0'];
				?>
				<div class="news-hero">
					<a href="<?php the_permalink() ?>" id="hero" style="background-image: url('<?php echo $url ?>');">
						<div >
							<div class="content">
								<div class="description">
									<h2><?php the_title(); ?></h2>
									<p><?php
									$content = get_the_content();
									$trimmed_content = wp_trim_words( $content, 32, '...' );
									echo $trimmed_content;
									?></p>
								</div>
							</div>
						</div>
					</a>
				</div>
				<?php endwhile; ?>
				<?php else : endif; ?>				
				<div class="content">
					<div class="col news-col three">
						<h3>Latest News</h3>
						<?php query_posts( array ( 'posts_per_page=4' ) ); ?>
						<ul>
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<a href="<?php the_permalink() ?>">
								<li>
									<?php if ( has_post_thumbnail() ) {
										$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'article-thumb' );
										$url = $thumb['0']; ?>
										<img src="<?php echo $url ?>" alt="<?php the_title(); ?>" />
									<?php } ?>
									<div class="news-item">
										<h4><?php the_title(); ?></h4>
										<p><?php
											$content = get_the_content();
											$trimmed_content = wp_trim_words( $content, 27, '...' );
											echo $trimmed_content;
										?></p>
									</div>
								</li>
							</a>							
							<?php endwhile; ?>
						</ul>
						<?php endif; ?>
						<a class="btn" href="/news/">View All</a>
					</div>
					<div class="col events-col one">
						<ul>
							<?php if ( is_active_sidebar( 'events-sidebar' ) ) : ?>
								<?php dynamic_sidebar( 'events-sidebar' ); ?>				
							<?php else : endif; ?>
						</ul>
					</div>
				</div>
			</div>
<?php get_footer(); ?>