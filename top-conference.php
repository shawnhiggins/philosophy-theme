<?php
/*
	Conference Navigation and Hero 
*/
?>

<a href="<?php bloginfo('url'); ?>" class="site-link">Back to <?php bloginfo( 'name' ); ?> Website</a>
<a href="#main-content" class="hidden skip">Skip to main content</a>
<?php
// If a child page, use this class
if ( $post->post_parent ) { ?>
<div id="container" class="not-landing">
<?php } 
// Else, use this class
else { ?>
<div id="container" class="landing">
<?php }
// If a child page, use this header
if ( $post->post_parent ) { ?>
	<header role="banner" class="top">
		<div class="content">
			<a href="<?php echo get_permalink($post->post_parent); ?>">
				<h2>
					<?php if(get_field('pre_title')) { ?><span class="subtitle"><?php the_field('pre_title'); ?></span><?php } ?>
					<?php echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent ); ?>
				</h2>
			</a>
		</div>
		<nav role="navigation" aria-labelledby="main navigation" class="desktop">
			<?php
			if ( $post->post_parent ) {
			    $children = wp_list_pages( array( 'title_li' => '', 'child_of' => $post->post_parent, 'echo' => 0, 'post_type' => 'conference', 'sort_column' => 'menu_order' ) );
			} else {
			    $children = wp_list_pages( array( 'title_li' => '', 'child_of' => $post->ID, 'echo' => 0, 'post_type' => 'conference', 'sort_column' => 'menu_order' ) );
			}
			if ( $children ) : ?>
		    <ul>
		        <?php echo $children; ?>
		    </ul>
			<?php endif; ?>
		</nav>
	</header>
	<?php if(get_field('page_type') == "location") { 
			if(get_field('google_map')) { ?>
			<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
			<?php $location = get_field('google_map');
				if( !empty($location) ): ?>
			<div class="map">
				<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
			</div>
			<?php endif; ?>
		<?php }
	} ?>
	<?php if ( has_post_thumbnail() ) {
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero' );
		$url = $thumb['0']; ?>
	<div id="hero" style="background-image: url('<?=$url?>');">
	</div>
	<?php }
} 
// If a parent page
else {
	if ( has_post_thumbnail() ) {
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero' );
		$url = $thumb['0']; ?>
	<div id="hero" style="background-image: url('<?=$url?>');">
		<div class="overlay">
		<?php } else { } ?>
			<header role="banner" class="top">
				<nav role="navigation" aria-labelledby="main navigation" class="desktop">
					<?php
					if ( $post->post_parent ) {
					    $children = wp_list_pages( array( 'title_li' => '', 'child_of' => $post->post_parent, 'echo' => 0, 'post_type' => 'conference', 'sort_column' => 'menu_order' ) );
					} else {
					    $children = wp_list_pages( array( 'title_li' => '', 'child_of' => $post->ID, 'echo' => 0, 'post_type' => 'conference', 'sort_column' => 'menu_order' ) );
					}
					if ( $children ) : ?>
				    <ul>
				        <?php echo $children; ?>
				    </ul>
					<?php endif; ?>
				</nav>
				<div class="content">
					<h1><?php if(get_field('pre_title')) { ?><span class="subtitle"><?php the_field('pre_title'); ?></span><?php } ?>
						<?php the_title(); ?></h1>
						
					<?php if(get_field('post_title')) { ?>
					<div class="details">
						<?php the_field('post_title'); ?>
					</div>
					<?php } ?>
					<?php if(get_field('button_text')) { ?>
					<a href="<?php the_field('button_link'); ?>" class="btn"><?php the_field('button_text'); ?></a>
					<?php } ?>
				</div>
				<nav role="navigation" aria-labelledby="main navigation" class="mobile">
					<?php
					if ( $post->post_parent ) {
					    $children = wp_list_pages( array( 'title_li' => '', 'child_of' => $post->post_parent, 'echo' => 0, 'post_type' => 'conference', 'sort_column' => 'menu_order' ) );
					} else {
					    $children = wp_list_pages( array( 'title_li' => '', 'child_of' => $post->ID, 'echo' => 0, 'post_type' => 'conference', 'sort_column' => 'menu_order' ) );
					}
					if ( $children ) : ?>
				    <ul>
				        <?php echo $children; ?>
				    </ul>
					<?php endif; ?>
				</nav>
			</header>
		<?php
		if ( has_post_thumbnail() ) { ?>
		</div>
	</div>
	<?php }
	}
	// Breadcrumbs
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
	}
	?>