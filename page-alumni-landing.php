<?php
/*
 Template Name: Alumni Landing Page
*/
?>
<?php get_header(); ?>
			<div class="content main" id="main-content">
				<header>
					<h1><?php the_title(); ?></h1>
				</header>
			</div>


                

            <div id="main-content" role="main">
				<div class="content">
					<?php
					if( have_rows('alumni_rows') ) :
						while ( have_rows('alumni_rows') ) : the_row();
							
							// For showing snippet from any page
							if( get_row_layout() == 'page_excerpt' ) 
								get_template_part('snippets/row', 'page');
					        
							// For showing list of recent post
							elseif( get_row_layout() == 'profile_list' ) 
								get_template_part('snippets/row', 'alumni');
					        
							// For showing list of recent post
							elseif( get_row_layout() == 'recent_posts' ) 
								get_template_part('snippets/col', 'posts');
							
							// For showing free form content
							elseif( get_row_layout() == 'content_block' ) 
								get_template_part('snippets/row', 'content');
							
							// For showing list of events from event widget
							elseif( get_row_layout() == 'upcoming_events' ) 
					       		get_template_part('snippets/col', 'events');
					       	
					       	// For showing a menu
					       	elseif( get_row_layout() == 'menu' ) 
					       			get_template_part('snippets/col', 'menu');
							
						endwhile;
					endif;
			    	?>
					</div>					
				</div>


<?php get_footer(); ?>


