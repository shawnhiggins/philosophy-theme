<?php
/*
	Basic Navigation and Hero 
*/
?>

<a href="#main-content" class="hidden skip">Skip to main content</a>
<div id="container">
	<header role="banner" class="top">
		<div class="content">
			<a href="<?php echo home_url(); ?>" rel="nofollow">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo.png" alt="UCLA" class="university-logo" /><span class="hidden">UCLA</span>
				<?php // H1 if homepage, H2 otherwise.
					if ( is_front_page() ) { ?>
				<h1><img src="<?php echo get_template_directory_uri(); ?>/library/images/dept-logo.png" alt="<?php the_field('department_name', 'option'); ?>" class="dept-logo" /><span class="hidden"><?php the_field('department_name', 'option'); ?></span></h1>
				<?php } else { ?>
				<h2><img src="<?php echo get_template_directory_uri(); ?>/library/images/dept-logo.png" alt="<?php the_field('department_name', 'option'); ?>" class="dept-logo" /><span class="hidden"><?php the_field('department_name', 'option'); ?></span></h2>
				<?php }?>
			</a>
            <?php get_search_form(); ?>
			<?php if(is_front_page() && get_field('enable_donation', 'option') == "enable") { ?>
			<div class="give-back">
				<?php if(get_field('link_type', 'option') == "internal") { ?>
				<a href="<?php the_field('donation_page', 'option'); ?>" class="btn give">
				<?php }?>
				<?php if(get_field('link_type', 'option') == "external") { ?>
				<a href="<?php the_field('donation_link', 'option'); ?>" class="btn give" target="_blank">
				<?php }?>
				<?php the_field('button_text', 'option'); ?></a>
				<?php if(get_field('supporting_text', 'option')) { ?>
				<span><?php the_field('supporting_text', 'option'); ?></span>
				<?php }?>
			</div>
			<?php }?>
		</div>
		<nav role="navigation" aria-labelledby="main navigation" class="desktop">
			<?php wp_nav_menu(array(
				'container' => false,
				'menu' => __( 'Main Menu', 'bonestheme' ),
				'menu_class' => 'main-nav',
				'theme_location' => 'main-nav',
				'before' => '',
				'after' => '',
				'depth' => 2,
			)); ?>
		</nav>
	</header>
	<?php 
		// Don't do any of the below if homepage
		if ( is_front_page() ) { }
		// Breadcrumb everywhere else
		elseif ( is_single() || is_category( $category ) || is_archive() ) { 
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			}
		}
		else {
			// Only show hero image on a page or post
			if ( has_post_thumbnail() && is_single() || has_post_thumbnail() && is_page() ) {
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero' );
				$url = $thumb['0']; ?>
	<div id="hero" style="background-image: url('<?=$url?>');">
		&nbsp;
	</div>
	<?php }
			// And show breadcrumb
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
			} 
		} 
	?>