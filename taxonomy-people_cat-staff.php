<?php get_header(); ?>
			<div class="content main" id="main-content">
				<header>
					<h1><?php single_cat_title(); ?></h1>
					<?php $category_description = category_description();
					if ( ! empty( $category_description ) )
					echo apply_filters( 'category_archive_meta', '<p>' . $category_description . '</p>' );
					?>
				</header>
				<div class="people-list">
					<ul <?php post_class('cf'); ?>>
					<?php $staff_loop = new WP_Query( array( 'people_cat' => 'staff', 'post_type' => 'people', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC') ); ?>
					<?php while ( $staff_loop->have_posts() ) : $staff_loop->the_post(); ?>
						<li class="person-item"><?php if(get_field('photo')) {
							$image = get_field('photo');
							if( !empty($image) ): 
								// vars
								$url = $image['url'];
								$title = $image['title'];
								// thumbnail
								$size = 'people-thumb';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ];
							endif; ?>
							<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
							<?php } else { ?>
							<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-people-logo-220.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
							<?php } ?>
							<dl>
								<span class="name"><?php the_title(); ?></span>									
								<span class="position"><?php the_field('position_title'); ?></span>
                                
							<div class="details">
								<?php if(get_field('office')) { ?>
									<span><strong>Office: </strong><?php the_field('office'); ?></span> | 
								<?php } ?>
								<?php if(get_field('phone_number')) { ?>
									<span><strong>Phone: </strong><?php the_field('phone_number'); ?></span> | 
								<?php } ?>
								<?php if(get_field('email_address')) { ?>
									<span class="email"><strong>E-mail: </strong><a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a></span>
								<?php } ?>
								<?php if(get_field('office_hours!')) { ?>
									<p><strong>Office Hours: </strong><?php the_field('office_hours'); ?></p>
								<?php } ?>
							</div>
                                
								<?php if(get_field('email_address!')) { ?>
								<dd class="email">
									<a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a>
								</dd>
								<?php } ?>						
                                <section class="bio">
                                    <?php the_content(); ?>
                                </section>
							</dl>
                            <div style="clear:both"></div>
						</li>
					<?php endwhile; ?>					
					</ul>
				</div>
			</div>
<?php get_footer(); ?>