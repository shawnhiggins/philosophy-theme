<?php
/*
 Template Name: Summer Courses
*/
?>
<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php 
						$term = get_field('quarter');
						$qt = $term->name;
					?>
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
						</section>
					</article>
					<?php // Courses ?>
					<?php $courses_loop = new WP_Query( 
						array( 'quarter' => "'$qt'", 'post_type' => 'courses', 'orderby' => 'title', 'order' => 'asc', 'posts_per_page' => -1, 'meta_query' => 
						array(
							array(
								'key' => 'program',
								'value' => 'summer',
							))
						));
					?>
					<h2 id="courses"><?php echo $qt; ?></h2>
					<?php if ( $courses_loop->have_posts() ) : while ( $courses_loop->have_posts() ) : $courses_loop->the_post(); ?>
					<h3><?php the_title(); ?></h3>
					<?php if(get_field('instructor_type') == "internal") { ?>
					<span class="instructors">
						<strong>Instructor: </strong>
						<?php $instructor = get_field('instructor'); ?>
						<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						<?php $courses_loop->reset_postdata(); ?>
						<?php endif; ?>
					</span>
					<?php }	?>
					<?php if(get_field('instructor_type') == "external") { ?>
					<span class="instructors">
						<?php if(get_field('additional_instructors')) { ?>
						<strong>Instructor: </strong><?php the_field('additional_instructors'); ?>
						<?php } ?>
					</span>
					<?php }	?>
					<?php if(get_field('instructor_type') == "both") { ?>
					<span class="instructors">
						<strong>Instructor: </strong>
						<?php $instructor = get_field('instructor'); ?>
						<?php if( $instructor ): $post = $instructor; setup_postdata( $post ); ?>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php $courses_loop->reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_instructors')) { ?>, <?php the_field('additional_instructors'); } ?>
					</span>
					<?php }	?>
					<?php the_content(); ?>
					<?php endwhile; else : ?>
					<p>There are no summer courses this quarter.</p>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
				</div>
				<?php get_sidebar(); ?>
			</div>
			<?php endwhile; else : ?>
			<?php endif; ?>
<?php get_footer(); ?>