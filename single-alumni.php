<?php get_header(); ?>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<header class="bio <?php if(get_field('person_type') == "alumni") { ?>alumni<?php } ?>">
					<div class="content">
						<?php if(get_field('photo')) {
							$image = get_field('photo');
							if( !empty($image) ): 
							// vars
							$url = $image['url'];
							$title = $image['title'];
							// thumbnail
							$size = 'people-large';
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ];
						endif; ?>
						<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
						<?php } else { ?>
						<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-people-logo-220.jpg" alt="Silhouette" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
						<?php } ?>
						<section>
							<h1 id="bio"><?php the_title(); ?></h1>                                            
                                <?php if(get_field('class_year')) { ?>
                                    <span class="year">                                                 
                                        Class of <?php the_field('class_year'); ?>
                                    </span>
                                <?php } ?>
                                <?php if(get_field('cohort_year')) { ?>
                                    <span class="year">
                                        Cohort of <?php the_field('cohort_year'); ?>
                                    </span>
                                <?php } ?>
                                <?php if(get_field('degree')) { ?>
                                    <span class="degree"> 
                                        <?php                                                                          
                                            $field = get_field_object('degree');
                                            $value = get_field('degree');
                                            $label = $field['choices'][ $value ];
                                        ?>
                                       <strong>Degree: </strong> <?php echo $label; ?>
                                    </span>
                                <?php } ?>
                                <?php if(get_field('other_degrees')) { ?>
                                    <span class="degree">                                                  
                                       <strong>Other degrees from elsewhere: </strong> <?php the_field('other_degrees'); ?>
                                    </span>
                                <?php } ?>                                   
                            
                            <?php if(get_field('current_occupation')) { ?>
                                <div class="current_occupation">
                                   <strong>Current occupation or plans after graduation: </strong> <?php the_field('current_occupation'); ?>
                                </div>
                            <?php } ?>          
							<div class="details">
                            <?php if (get_field('contact_me') == "yes"): ?>
                                <?php if(get_field('email_address')) { ?>
                                    <span><strong>E-mail: </strong><a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a></span>
                                <?php } ?>
                                <?php if(get_field('mailing_address')) { ?>
                                    <span><strong>Address: </strong><?php the_field('mailing_address'); ?></span>
                                <?php } ?>
                            <?php endif; ?>
                            <?php if(get_field('personal_website')) { ?>
                                <p><a href="<?php the_field('personal_website'); ?>" class="link">Personal Website</a></p>
                            <?php } ?>
							</div>
							<?php the_content(); ?>
						</section>
					</div>
				</header>
				<div class="content main">
					<div class="col" id="main-content" role="main">
                        <?php if(get_field('undergrad_or_grad_btn')== 'undergrad' ):?>
                            <?php if(get_field('favorite_course')) { ?>
                            <section id="favorite_course">
                                <h2>What were your favorite philosophy courses at UCLA?</h2>
                                <?php the_field('favorite_course'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('share_story')) { ?>
                            <section id="share_story">
                                <h2>Do you have a good story or two about the department?</h2>
                                <?php the_field('share_story'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('philosophical_topics')) { ?>
                            <section id="philosophical_topics">
                                <h2>Are there any philosophical issues, readings, or topics that have stayed with you since graduation?</h2>
                                <?php the_field('philosophical_topics'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('articles')) { ?>
                            <section id="articles">
                                <h2>Have you read any philosophy recently that you would recommend?</h2>
                                <?php the_field('articles'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('first_job')) { ?>
                            <section id="first_job">
                                <h2>What was your first job or endeavor after UCLA?</h2>
                                <?php the_field('first_job'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('learned')) { ?>
                            <section id="learned">
                                <h2>What lessons or skills from philosophy do you use in your career?</h2>
                                <?php the_field('learned'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('advice')) { ?>
                            <section id="advice">
                                <h2>Do you have advice for current students or recent graduates about how to take advantage of and continue their philosophical education?</h2>
                                <?php the_field('advice'); ?>
                            </section>
                            <?php } ?>                        
                        <?php endif; ?>
						
                        <?php if(get_field('undergrad_or_grad_btn')== 'grad' ):?>
                            <?php if(get_field('dissertation')) { ?>
                            <section id="dissertation">
                                <h2>What was your dissertation title and topic?</h2>
                                <?php the_field('dissertation'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('seminar_influence')) { ?>
                            <section id="seminar_influence">
                                <h2>What seminar stood out and influenced your thinking?</h2>
                                <?php the_field('seminar_influence'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('share_story')) { ?>
                            <section id="share_story">
                                <h2>Do you have a good story or two about the department?</h2>
                                <?php the_field('share_story'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('intellectual_approach')) { ?>
                            <section id="intellectual_approach">
                                <h2>What, if anything, about the UCLA department’s culture and approach to philosophy has influenced your intellectual approach?</h2>
                                <?php the_field('intellectual_approach'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('philosophical_interests')) { ?>
                            <section id="philosophical_interests">
                                <h2>How have your philosophical interests changed since you were at UCLA?</h2>
                                <?php the_field('philosophical_interests'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('first_job')) { ?>
                            <section id="first_job">
                                <h2>What was your first job or endeavor after leaving UCLA?</h2>
                                <?php the_field('first_job'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('nonacademia_career')) { ?>
                            <section id="nonacademia_career">
                                <h2>If your current career is outside of academia, have you found your philosophical training useful and if so, in what way?</h2>
                                <?php the_field('nonacademia_career'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('academia_career')) { ?>
                            <section id="academia_career">
                                <h2>If your current career is inside of academia, what’s your favorite course to teach right now? Do you have a recent publication you’d like to mention?</h2>
                                <?php the_field('academia_career'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('favorite_publications')) { ?>
                            <section id="favorite_publications">
                                <h2>Any all-time favorite philosophical articles or books you would recommend? Any new discoveries?</h2>
                                <?php the_field('favorite_publications'); ?>
                            </section>
                            <?php } ?>
                            <?php if(get_field('advice')) { ?>
                            <section id="advice">
                                <h2>Do you have advice for current students or recent graduates about how to take advantage of and continue their philosophical education?</h2>
                                <?php the_field('advice'); ?>
                            </section>
                            <?php } ?>                        
                        <?php endif; ?>
					</div>
                    
			
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php								
								// If an Alumni subpage
								
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Alumni & Friends', 'bonestheme' ),
									   	'menu_class' => 'alumni-nav',
									   	'theme_location' => 'alumni-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Alumni & Friends</h3> <ul>%3$s</ul>'
									));
								
							?>
						</nav>
					</div>
				</div>
				</div>
			<?php endwhile; ?>
			<?php else : endif; ?>
<?php get_footer(); ?>