
<section class="interview">
    <?php session_start();

       // $page = get_field('select_pages');
       // $_SESSION['select_pages'] = $page;            
       // $_SESSION['page_type'] = get_field('page_type');
//echo $_SESSION['page_type'];

        if( have_rows('interview') ) {
        while ( have_rows('interview') ) { the_row();
                // This is for the Home blog page  
                
                if(($_SESSION['select_pages']) || ($_SESSION['page_type'] == "interview-blog") ){ 
                    $i++;
                    if( $i > 2 ):
                        break; 
                    endif; 

                }
                                          
      
        // For showing snippet from any page
        if( get_row_layout() == 'interview_block' ) {
        //get_template_part('snippets/col', 'page');
            ?>


    <?php
            $interviewer_first_name = get_sub_field('interviewer_first_name');
            $interviewer_last_name = get_sub_field('interviewer_last_name');
            $interviewer = $interviewer_first_name .' '. $interviewer_last_name;
            $interviewer_fn_first_letter = substr($interviewer_first_name, 0, 1);
            $interviewer_ln_first_letter = substr($interviewer_last_name, 0, 1);
            $iver_initials = $interviewer_fn_first_letter . $interviewer_ln_first_letter;

            $interviewee_first_name = get_sub_field('interviewee_first_name');
            $interviewee_last_name = get_sub_field('interviewee_last_name');
            $interviewee = $interviewee_first_name .' '. $interviewee_last_name;
            $interviewee_fn_first_letter = substr($interviewee_first_name, 0, 1);
            $interviewee_ln_first_letter = substr($interviewee_last_name, 0, 1);
            $ivee_initials = $interviewee_fn_first_letter . $interviewee_ln_first_letter;

            $about_interview = get_sub_field('about_interviewee');
            ?>


    <div class="brief-intro">
        <span class="interview-photo">
            <?php if(get_sub_field('interviewee_photo')) {
            $image = get_sub_field('interviewee_photo');
            if( !empty($image) ): 
            // vars
            $url = $image['url'];
            $title = $image['title'];
            // thumbnail
            $size = 'newsletter-people-photo';
            $thumb = $image['sizes'][ $size ];
            $width = $image['sizes'][ $size . '-width' ];
            $height = $image['sizes'][ $size . '-height' ];
        endif; ?>
        <img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
        <?php } else { ?>
            <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla_interview_logo460.jpg" alt="Silhouette" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
        <?php } ?>
            <?php if($interviewee){ echo '<h4>'. $interviewee .'</h4>';} ?>
        </span>

        <span class="interview-photo">
            <?php if(get_sub_field('interviewer_photo')) {
            $image = get_sub_field('interviewer_photo');
            if( !empty($image) ): 
            // vars
            $url = $image['url'];
            $title = $image['title'];
            // thumbnail
            $size = 'newsletter-people-photo';
            $thumb = $image['sizes'][ $size ];
            $width = $image['sizes'][ $size . '-width' ];
            $height = $image['sizes'][ $size . '-height' ];
        endif; ?>
        <img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
        <?php } else { ?>
            <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla_interview_logo460.jpg" alt="Silhouette" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
        <?php } ?>
            <?php if($interviewer){ echo '<h4>'. $interviewer .'</h4>';} ?>
        </span>
        <span class="interview-about">
            <?php echo $about_interview; ?>                                
        </span>                               

    </div>
    <?php

    // For showing question and answers
    } elseif( get_row_layout() == 'question_answers_block' ) {
    //get_template_part('snippets/col', 'posts');
        $question = get_sub_field('question');
        $answer = get_sub_field('answer');
        if ($question){
        echo '<h6>' . $iver_initials . ': '. $question . '</h6>';
        }
        if ($answer){
            // this will strip the first <p> to help merge intials and answer together.
            $string = preg_replace('~<p>(.*?)</p>~is', '$1', $answer, /* limit */ 1); 
            
            echo '<div class="answer"><span class="initials">' . $ivee_initials . '</span>: '. $string . '</div>';
 
        }

    // For showing quotes
    } elseif( get_row_layout() == 'quotes_block' ) {
    //get_template_part('snippets/col', 'posts');
        $quote = get_sub_field('quote');
        echo '<blockquote> <p>'.$quote .'</p></blockquote>';
         }
        }
    }

        ?>

</section>