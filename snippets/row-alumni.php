<section class="alumni tax-alumni_cat">
   <h3><?php the_sub_field('list_title'); ?></h3>
    <div class="people-list">
        <ul <?php post_class('cf'); ?>>
            <?php $core_loop = new WP_Query( array( 'alumni_cat' => 'undergrad, undergraduate, grad, graduate', 'post_type' => 'alumni', 'posts_per_page' => -1, 'showposts' => 4 ,'orderby' => 'rand')); ?>
            <?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
                <li class="person-item<?php $areas = get_field('area_of_study'); if( $areas ): foreach( $areas as $area ): ?> <?php echo $area->slug; ?><?php endforeach; endif;?><?php $languages = get_field('language_of_study'); if( $languages ): foreach( $languages as $language ): ?> <?php echo $language->slug; ?><?php endforeach; endif;?>">
                    <a href="<?php the_permalink(); ?>">
                        <?php // if there is a photo, use it
                        if(get_field('photo')) {
                            $image = get_field('photo');
                            if( !empty($image) ): 
                                // vars
                                $url = $image['url'];
                                $title = $image['title'];
                                // thumbnail
                                $size = 'people-thumb';
                                $thumb = $image['sizes'][ $size ];
                                $width = $image['sizes'][ $size . '-width' ];
                                $height = $image['sizes'][ $size . '-height' ];
                        endif; ?>
                        <img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
                        <?php // otherwise use a silhouette 
                        } else { ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-people-logo-220.jpg" alt="A photo of <?php the_title(); ?>" class="photo <?php if(get_field('corner_style', 'option') == "circle") { ?>circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
                        <?php } ?>
                        <dl>
                            <dt class="name"><?php the_title(); ?></dt>
                            <?php if(get_field('class_year')) { ?>
                            <dd class="year">
                                Class of <?php echo get_field('class_year'); ?>
                            </dd>
                            <?php } ?>
                            <?php if(get_field('degree')) { ?>
                                <dd class="degree"> 
                                    <?php                                                                          
                                        $field = get_field_object('degree');
                                        $value = get_field('degree');
                                        $label = $field['choices'][ $value ];
                                    ?>
                                   <strong>Degree: </strong> <?php echo $label; ?>
                                </dd>
                            <?php } ?>
                            <?php if(get_field('current_occupation')) { ?>
                                <dd class="occupation">
                                   <strong>Occupation: </strong> <?php the_field('current_occupation'); ?>
                                </dd>
                            <?php } ?>
                            <?php if (get_field('contact_me') == "yes"): ?>
                                <?php if($phone =='yes'){
                                    if(get_field('phone_number')) { ?>
                                <dd class="phone">
                                    <strong>Phone: </strong><?php the_field('phone_number'); ?>
                                </dd>
                                <?php } } ?>	
                                <?php if($email=='yes') {
                                    if(get_field('email_address')) { ?>
                                <dd class="email">
                                    <a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a>
                                </dd>
                                <?php } }?>
                            <?php endif; ?>
                        </dl>
                    </a>
                </li>
            <?php endwhile; ?>					
            </ul>  
        </div>
	<?php if(get_sub_field('show_button') == "yes") { ?>
	<a class="btn" href="<?php the_sub_field('button_link'); ?>"><?php the_sub_field('button_text'); ?></a>
	<a class="btn" href="<?php echo home_url(); ?>/alumni/undergrad/">Undergraduate List</a>
	<a class="btn" href="<?php echo home_url(); ?>/alumni/grad/">Graduate List</a>
	<?php } ?>   
	<?php wp_reset_postdata(); ?>
    </section>