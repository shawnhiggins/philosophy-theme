<section class="arrivals">
    <?php

                // For showing snippet from Faculty Arrivals and/or Departures
                if( get_row_layout() == 'faculty_block' ) {
                    //get_template_part('snippets/col', 'posts');
                    $fac_section_title = get_sub_field('section_title');
                    $fac_content = get_sub_field('fac_content');

                    if ($fac_section_title){ echo '<h3>' . $fac_section_title . '</h3>'; }else{ echo '<h3>Faculty</h3>'; }
                    if ($fac_content){ echo $fac_content;  }
                }

                // For showing snippet from Graduate Students
                if( get_row_layout() == 'grad_block' ) {
                    //get_template_part('snippets/col', 'posts');
                    $grad_section_title = get_sub_field('grad_section_title');
                    $grad_content = get_sub_field('grad_content');

                    if ($grad_section_title){ echo '<h3>' . $grad_section_title . '</h3>'; } else { echo '<h3>Graduate Students</h3>';}
                    if ($grad_content){ echo $grad_content;  }

                    // For showing snippet to show Graduate Student year and list                              
                    if( have_rows('year_graduate_info') ) {
                        while ( have_rows('year_graduate_info') ) { the_row();

                            $grad_year = get_sub_field('grad_year');
                            $grad_list = get_sub_field('grad_list');

                            if ($grad_year){ echo '<h4>' . $grad_year . '</h4>'; }
                            if ($grad_list){ echo $grad_list;  }
                        }
                    }
                }

                // For showing snippet from Staff Arrivals and Departures
                if( get_row_layout() == 'staff_block' ) {

                    $staff_section_title = get_sub_field('staff_section_title');
                    $staff_content = get_sub_field('staff_content');

                    if ($staff_section_title){ echo '<h3>' . $staff_section_title . '</h3>'; }
                    if ($staff_content){ echo $staff_content;  }

                    // For showing snippet to show Graduate Student year and list                              
                    if( have_rows('staff_arrivals_departures') ) {
                        while ( have_rows('staff_arrivals_departures') ) { the_row();

                            $staff_year = get_sub_field('the_year');
                            $staff_arrivals = get_sub_field('staff_arrivals');
                            $staff_departures = get_sub_field('staff_departures');

                            if ($staff_year){ echo '<h4>'. $staff_year .'</h4>';  }
                            if ($staff_arrivals){ echo '<h5>Arrivals: </h5>'. $staff_arrivals; }
                            if ($staff_departures){ echo '<h5>Departures: </h5>'. $staff_departures;  }
                        }
                    }
                }
            

        ?>

</section>