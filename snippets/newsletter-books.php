<?php session_start(); ?>


<?php if(get_field('recent_books')) { ?>

    <section id="books">
        <?php $book = get_field('recent_books'); ?>
        <ul class="book-list">
            <? if( $book ): ?>
            <?php foreach( $book as $post): ?>
            <?php 
                // This is for the Home blog page                     
                if($_SESSION['select_pages']){ 
                    $i++;
                    if( $i > 3 ):
                        break; 
                    endif; 

                }; ?>
            
            
            <?php setup_postdata($post); ?>
            <li>
                <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                <?php if(get_field('book_cover')) {
                    $image = get_field('book_cover');
                    if( !empty($image) ): 
                        // vars
                        $url = $image['url'];
                        $title = $image['title'];
                        // thumbnail
                        $size = 'large-book';
                        $thumb = $image['sizes'][ $size ];
                        $width = $image['sizes'][ $size . '-width' ];
                        $height = $image['sizes'][ $size . '-height' ];
                    endif; ?>
                    <img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> book cover" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="cover wp-post-image" />
                    <?php } else { ?>
                        <div class="custom-cover cover">
                            <span class="title"><?php the_title(); ?></span>
                        </div>
                    <?php } ?>
                </a>
                <dl>
                    <dt class="title">
                        <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                            <?php the_title(); ?>

                            <?php if(get_field('subtitle')) { ?>
                            <span class="subtitle">
                            : <?php the_field('subtitle'); ?>
                            </span>
                            <?php } ?>
                        </a>
                    </dt>
                    <dd class="author">
                        <?php $author = get_field('author'); ?>
                        <? if( $author ): ?>
                        <?php foreach( $author as $post): ?>
                        <?php setup_postdata($post); ?>
                        <a href="<?php the_permalink(); ?>" class="author-name"><strong>– </strong><?php the_title(); ?></a><?php endforeach; ?><?php wp_reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_authors')) { ?>, <?php the_field('additional_authors'); ?>
                        <?php } ?>
                    </dd>
                </dl>
            </li>
            <?php endforeach; ?>
            <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </ul>
    </section>
                
<?php } ?>