<?                   session_start();
                   // get_template_part('snippets/newsletter', 'parent');
                    
                    $display = get_field('display');
					//$people_details = get_field('people_details');
					if( in_array('letter', $display) ) { 
						$letter = 'yes';
					} 
					if( in_array('interviews', $display) ) {
						$interviews = 'yes';
					} 
					if( in_array('qa', $display) ) {
						$qa = 'yes';
					}
					if( in_array('events', $display) ) {
						$events = 'yes';
					}
					if( in_array('awards', $display) ) {
						$awards = 'yes';
					}
					if( in_array('memoriam', $display) ) {
						$memoriam = 'yes';
					}
					if( in_array('arrivals', $display) ) {
						$arrivals = 'yes';
					}
                    ?>
                
                
                
                
                
                
            
                
                
        <?php $page = get_field('select_pages');

            $_SESSION['select_pages'] = $page;
?>
        <ul class="blog items-list">
            <? if( $page ): ?>
            <?php foreach( $page as $post): ?>
            <?php setup_postdata($post); ?>
            <li>
                
                
                <dl>
                    <dt class="title">
                        <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                            <h1><?php if(have_rows('interview')) { echo "Interview with "; } ?><?php the_title(); ?></h1>
                        </a>
                    </dt>
                    <?php 
                    $content = get_the_content();

                if($content){?>
                    
                <?php if($letter == 'yes') { ?>
                    <dd class="news-item">
                        <p>
                            <?php 
                            $trimmed_content = wp_trim_words( $content, 100, '...' );
                            echo $trimmed_content; ?>
                        </p>                        
                    </dd>                    
                <?php } ?>
                    
                <?php } ?>
                    
                </dl>
                
                <?php 

                        $i = 0;
                    if($interviews == 'yes') {
                        //if( have_rows('interview') ) {
                       // while ( have_rows('interview') ) { the_row();
                                                          
                         //   $i++;
                         //   if( $i > 2 ):
                         //       break; 
                         //   endif; 
                                                          
                            get_template_part('snippets/newsletter', 'interview');
                           // }
                       // }
                    } if($qa == 'yes') {
                        
    
                        if( have_rows('philosophers_q_a') ) {
                        while ( have_rows('philosophers_q_a') ) { the_row();
                                                                 
                            $i++;
                            if( $i > 1 ):
                                break; 
                            endif; 

                            get_template_part('snippets/newsletter', 'qa');
                                                    }
                        }
                    } 
                    if($memoriam == 'yes') {
                          
                        if( have_rows('memoriam') ) {
                            while ( have_rows('memoriam') ) { the_row();
                                                                 
                            $i++;
                            if( $i > 1 ):
                                break; 
                            endif; 
            
                        get_template_part('snippets/newsletter', 'memoriam');
                                                            }
                        }
                    } 
                    if($arrivals == 'yes') {
                        
                    if( have_rows('arrivals_departures') ) {
                        while ( have_rows('arrivals_departures') ) { the_row();
                                                                 
                            $i++;
                            if( $i > 1 ):
                                break; 
                            endif;
                                                        
                        get_template_part('snippets/newsletter', 'arrivals');
                                                                   }
                        }
                    } 
                    if($awards == 'yes') {
                        if( have_rows('awards') ) {
                        while ( have_rows('awards') ) { the_row();
                                                                 
                            $i++;
                            if( $i > 1 ):
                                break; 
                            endif;
                        get_template_part('snippets/newsletter', 'awards');
                                                      }
                        }
                    } 
                    if($recent-books == 'yes') {
                        $my_ID = $post->ID;                        
                        get_template_part('snippets/newsletter', 'books');
                    }
                        ?>
                    
                <?php if(get_field('home_page_link', $my_ID)){?>
                    <a href="<?php the_field('home_page_link', $my_ID) ?>" title="<?php the_title_attribute(); ?>">
                        READ MORE >
                    </a>
                <?php } else {?>
                        <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                            READ MORE >
                        </a>
                <?php } ?>
            </li>
            <?php endforeach; ?>
            <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </ul>