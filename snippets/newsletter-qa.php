<section class="qa">

            <section class="qa-items">
            <?php

                //get_template_part('snippets/col', 'posts');
                $question = get_sub_field('question');
                $answer = get_sub_field('answer');
                $persons_name = get_sub_field('persons_name');
                $degree = get_sub_field('their_degree');
                $graduated = get_sub_field('year_of_graduation');

                if ($question){
                    echo '<h3> Q: ' . $question . '</h3>';
                }
                if ($answer){
                    echo '<p> A: ' . $answer . '</p>';
                }
                if (($persons_name) || ($degree) || ($graduated)){
                    echo '<p> – ' . $persons_name . ', ' . $degree . ', Class of ' . $graduated .  '</p>';
                }
                ?>

            </section>
</section>