<section class="memoriam">
  

            <section class="items">
            <?php
                $person_name = get_sub_field('name');
                $memory_of = get_sub_field('memory_of');

                if ($person_name){
                    echo '<h3>' . $person_name . '</h3>';
                }
                if ($memory_of){
                    echo  $memory_of;
                }

                ?>

            </section>

</section>