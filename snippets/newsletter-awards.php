<section class="awards">    
    <?php
        
                //get_template_part('snippets/col', 'posts');
                $award_year = get_sub_field('award_year');
                $awardees = get_sub_field('awardees');

                if ($award_year){ echo '<h3>' . $award_year . '</h3>'; }
                if ($awardees){ echo $awardees;  }

    ?>
</section>