<?php get_header(); ?>
			<div class="content main">
				<div class="col" id="main-content" role="main">

					<?php if (is_category()) { ?>
					<h1 class="archive-title">
						<?php single_cat_title(); ?>
					</h1>
					<?php } elseif (is_tag()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Articles Tagged:', 'bonestheme' ); ?></span> <?php single_tag_title(); ?>
					</h1>
					<?php } elseif (is_author()) {
						global $post;
						$author_id = $post->post_author;
					?>
					<h1 class="archive-title">
						<span><?php _e( 'Articles By:', 'bonestheme' ); ?></span> <?php the_author_meta('display_name', $author_id); ?>
					</h1>
					<?php } elseif (is_day()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Daily Archives:', 'bonestheme' ); ?></span> <?php the_time('l, F j, Y'); ?>
					</h1>
					<?php } elseif (is_month()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Monthly Archives:', 'bonestheme' ); ?></span> <?php the_time('F Y'); ?>
					</h1>
					<?php } elseif (is_year()) { ?>
					<h1 class="archive-title">
						<span><?php _e( 'Yearly Archives:', 'bonestheme' ); ?></span> <?php the_time('Y'); ?>
					</h1>
					<?php } ?>
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">
						<h3 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
						<span class="publish-date"><strong>Published:</strong> <?php echo get_the_date(); ?></span>
						<section class="entry-content cf">
							<?php the_post_thumbnail( 'content-width' ); ?>
							<?php the_excerpt(); ?>
							<a href="<?php the_permalink() ?>" class="btn">Read More</a>
						</section>
					</article>

					<?php endwhile; ?>
					
					<?php bones_page_navi(); ?>
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<section>
							<p>There is nothing available to show here at this time. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>